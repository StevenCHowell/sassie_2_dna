import sys
import os

try:
    place_of_create_sasmol_cpp = os.path.join(
        '..', 'cppExtensions_buildingBlock', 'sasmol_as_cpp')
    sys.path.append(place_of_create_sasmol_cpp)
    from sasmol_as_cpp import create_sasmol_as_cpp

    place_of_get_bead_masks_in_cpp = os.path.join(
        '..', 'cppExtensions_buildingBlock', 'get_bead_masks_in_cpp')
    sys.path.append(place_of_get_bead_masks_in_cpp)
    from get_bead_masks import get_bead_masks_in_cpp
except:
    print 'cpp sasmol not available'

import numpy
import time
import sassie.sasmol.sasmol as sasmol


class var_object():

    def __init__(self, parent=None):
        pass


def get_bead_masks(other_self, group_number, nvars):
    # Get input:
    flexible_mol = other_self.group_flexible_molecules[group_number]
    bp_in_each_bead = nvars.base_pairs_in_each_bead[group_number]
    number_beads = nvars.number_beads[group_number]
    chain1_res_a = nvars.resids1[group_number][0]
    chain2_res_b = nvars.resids2[group_number][0]

    number_base_pairs = nvars.resids1[group_number].size
    [nucleic_segname1, nucleic_segname2] = nvars.dna_segnames

    '''
    tic = time.time()
    cpp_object = create_sasmol_as_cpp(flexible_mol)
    bead_masks = get_bead_masks_in_cpp(cpp_object, bp_in_each_bead, number_beads, chain1_res_a, chain2_res_b, number_base_pairs, nucleic_segname1, nucleic_segname2)
    toc = time.time() - tic
    print '\nCreating the bead filters took %0.3f seconds' % toc
    # print 'CPP return bead_masks: '
    # print bead_masks.tolist()
    nvars.nucleic_acid_bead_masks[group_number] = bead_masks    
    '''
    bead_masks = []
    tic = time.time()
    for j in xrange(number_beads):
        if bp_in_each_bead[j] > 1:
            # create the basis filters fore ach bead
            if j + 1 == number_beads:
                # accomodate for a non-divisible number of residues
                bp_in_each_bead[j] = number_base_pairs - bp_in_each_bead[j] * j

            # Get the atoms from DNA strand 1
            chain1_res_b = chain1_res_a + bp_in_each_bead[j]
            chain1_bead_filter = ("((resid[i] >= " + str(chain1_res_a) +
                                  " and resid[i] < " + str(chain1_res_b) +
                                  ") and (segname[i] == '" +
                                  nucleic_segname1 + "')) or ")
            chain2_res_a = chain2_res_b - bp_in_each_bead[j]
            chain2_bead_filter = ("((resid[i] > " + str(chain2_res_a) +
                                  " and resid[i] <= " + str(chain2_res_b) +
                                  ") and (segname[i] == '" +
                                  nucleic_segname2 + "'))")

            # setup for next iteration
            chain1_res_a = chain1_res_b
            chain2_res_b = chain2_res_a
        else:
            chain1_bead_filter = ("((resid[i] == " + str(chain1_res_a) +
                                  ") and (segname[i] == '" +
                                  nucleic_segname1 + "')) or ")
            chain2_bead_filter = ("((resid[i] == " + str(chain2_res_b) +
                                  ") and (segname[i] == '" +
                                  nucleic_segname2 + "'))")

            # setup for next iteration
            chain1_res_a += 1
            chain2_res_b -= 1

        bead_filter = chain1_bead_filter + chain2_bead_filter
        print 'bead_filter =:', bead_filter
        # create the bead masks to select the atoms from the aa mol
        error, bead_mask = flexible_mol.get_subset_mask(bead_filter)
        # print bead_filter
        # print bead_mask.tolist()
        #handle_error(other_self, error)

        # store the mask for the reverse coarse-graining
        bead_masks.append(bead_mask)
    # print 'Python return bead_masks: '
    # print numpy.array(bead_masks).tolist()
    toc = time.time() - tic
    nvars.nucleic_acid_bead_masks[group_number] = bead_masks
    print '\nCreating the bead filters took %0.3f seconds' % toc

if __name__ == '__main__':

    other_self = var_object()
    nvars = var_object()

    nvars.flexible_mol = sasmol.SasMol(0)

    flexible_mol = nvars.flexible_mol

    flexible_mol.read_pdb('new_c11_tetramer.pdb')

    dna_segnames = ['DNA1', 'DNA2']
    dna_molecule = sasmol.SasMol(1)
    dna_filter = "(segname[i] ==  '%s'  or segname[i] ==  '%s' )" % (
        dna_segnames[0], dna_segnames[1])

    error, dna_mask = flexible_mol.get_subset_mask(dna_filter)
    error = flexible_mol.copy_molecule_using_mask(dna_molecule, dna_mask, 0)

    # ~~~~ Input parameters ~~~~
    other_self.group_flexible_molecules = [dna_molecule]
    # quick version: ~11s
    nvars.number_beads = [5]
    nvars.resids1 = [numpy.array(range(1, 6))]
    nvars.resids2 = [numpy.array(range(5, 0, -1))]
    nvars.dna_segnames = dna_segnames
    # ~~~~~~>  Uncomment this to see how long parsing a large file takes:
    '''
    nvars.number_beads            = [693]
    nvars.resids1                 = [numpy.array(range(1,694))]
    nvars.resids2                 = [numpy.array(range(693,0,-1))]
    '''

    nvars.base_pairs_in_each_bead = [
        numpy.ones(nvars.number_beads, dtype=int) * 1]
    group_number = 0
    nvars.nucleic_acid_bead_masks = [None]

    tic = time.time()
    get_bead_masks(other_self, group_number, nvars)

    toc = time.time() - tic
    print '\nCreating the bead filters took %0.3f seconds' % toc

    print '\nHere are the masks:'
    for i in xrange(nvars.number_beads[0]):
        n_atoms = numpy.sum(nvars.nucleic_acid_bead_masks[0][i])
        # print 'nvars.nucleic_acid_bead_masks[0]['+str(i)+'] = ',
        # nvars.nucleic_acid_bead_masks[0][i].tolist(), '; total # atoms:',
        # n_atoms

        print '\n\m/ >.< \m/'
