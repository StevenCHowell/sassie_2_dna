
\title{\singlespacing Dynamic Conformations of Nucleosome Arrays in Solution from Small-Angle X-ray Scattering}
\author{by Steven C.~Howell}
\date{}
\maketitle
\thispagestyle{empty}
\centerline{B.S.~in Physics, April 2009, Brigham Young University}
\centerline{M.S.~in Physics, May 2013, The George Washington University}
\vspace{1in}
\centerline{A Dissertation submitted to}
\vspace{0.5in}
\begin{center}
The Faculty of\\The Columbian College of Arts and Sciences \\ of The George
Washington University\\ in partial fulfillment of the requirements\\ for the degree
of Doctor of Philosophy\\[1in]
January 31, 2016\\[1in]
Dissertation directed by\\[\baselineskip]
Xiangyun Qiu\\Assistant Professor of Physics
\end{center}
% display page numbers in the footer and centered. Start with roman numerals %
\pagestyle{plain}
\setcounter{page}{1}
\pagenumbering{roman}
\newpage
\doublespacing
\noindent 
The Columbian College of Arts and Sciences of The George Washington University certifies that Steven Craig Howell has passed the Final Examination for degree of Doctor of Philosophy as of November 20, 2015.
This is the final and approved form of the dissertation.\\
\begin{center}
{\singlespacing \LARGE Dynamic Conformations of Nucleosome Arrays in Solution from Small-Angle X-ray Scattering}\\{Steven C.~Howell}
\end{center}
\vspace{0.5in}
Dissertation Research Committee:\\

\singlespacing
Xiangyun Qiu, Assistant Professor of Physics, Dissertation Director\\%[\baselineskip]

Mark Reeves, Professor of Physics, Committee Member\\%[\baselineskip]

Joseph E.~Curtis, Research Chemist, National Institute of Standards and\\
\indent Technology, Committee Member\\%[\baselineskip]
% removed \noindent commands so these were indented correctly
\newpage
\vspace*{\fill}
\begin{center}
\singlespacing
$\copyright$ Copyright 2015 by Steven C.~Howell\\ All rights reserved
\end{center}
\vspace*{\fill}
\newpage
% \section{\protect \centering{Dedication}}
\section{Dedication}
\doublespacing
\begin{center}
  To my father, who taught me to work hard, \\
  my mother, who taught me to never give less than my best, \\
  and above all, to my wife, who taught me to live and to love.  
\end{center}
\newpage
%\section{\protect \centering Acknowledgments}
\section{Acknowledgments}
I am indebted to countless family members, mentors, and friends who have supported me throughout my pursuit of this degree.
Their influence inspires and motivates me to succeed, while also making that success worthwhile.
As I cannot recognize all who have helped me to this point, I will specifically mention those who have played a significant role during my graduate studies.

First and foremost, I would like to thank my family.
They have always been supportive throughout my academic and other pursuits.
I am especially grateful to my mother and my in-laws for the time their help caring for my boys, who were both born during my graduate studies.
To my wife, Rachael, I do not know how to fully express my deepest appreciation for her tremendous encouragement, support, and confidence.
My will to persist through challenges originates in the perspective provided by her and our two boys.

I would also like to express my utmost gratitude and respect to my advisor, Xiangyun Qiu.
The time I have spent working as part of his group has taught me how to be a successful research scientist.
In our interactions, I have experienced unwavering confidence, limitless patience, and above all, his desire that I succeed.

I am also incredibly grateful for the fantastic researchers I have been able to collaborate with; this research would not have been possible without their combined efforts.
Specifically, my thanks goes out to Kurt Andresen, for helpful scientific discussions and assistance in performing SAXS experiments. 
To those from NIH, Yawen Bai and Bing-Rui Zhou, thank you for providing the 4$\times$167 gH5 and 12$\times$167 nucleosome samples.
To those from Purdue University, Chongli Yuan, Isabel Jimenez-useche, and Agnes Mendonca, thank you for providing DNA templates and assisting in preparing histone octamers.
To the researchers at GWU, Fuyou Ke, Wei Meng, Carina Lorenzen, Suzanne Arnott, Raju Timsina, and Ben Kopchick, it has been a pleasure to work with you.
I would also like to recognize Joseph Curtis, the University of Tennessee, and all the scientist at the \href{https://www.ncnr.nist.gov/}{NIST Center for Neutron Research}, for supporting me as a guest researcher and providing the computing resources used for this work.

During my time at GWU, there have been many others who have had a positive influence on me.
I would like to thank Harald Grie{\ss}hammer and Andre Alexandru for pushing me to excel academically as a new graduate student.
I owe a great deal to Qi Xia, Kevin Sykora, Marty McHugh, and Trevor Balint for the opportunity to work together in mastering the core physics concepts.
I am grateful to Adam Hughes for informative scientific and programming discussions as well as introducing me to several valuable Python resources.
I also want to thank Chris Verhaaren for reviewing this dissertation and also for his friendship and helpful scientific discussions.

To the broader community, thank you to those unnamed individuals who have contributed to my success.
I appreciate all those from \href{https://www.researchgate.net/profile/Steven_Howell2?ev=hdr_xprf&_sg=1xgGWgPSFokPBvmbeN_mxI0fUr7yU_gan22zU_mMPpGSTSotb7kk5dO1FXPexJmc}{Research Gate} and the \href{http://stackexchange.com/users/2098123/stvn66}{Stack Exchange communities} who provided helpful input in response to my programming, statistics, chemistry, Linux, \LaTeX, and other queries.  
I owe a huge thanks to \href{http://wingware.com}{Wingware Python IDE} for the complimentary professional license they provided as this application became an integral part of software development and data analysis.
Additionally, I am grateful to the developers of the many open-source software packages that have made my research and programming possible.

I would lastly like to express my appreciation for the funding sources that facilitated my dissertation research, including the facilities and software I used.
I am sincerely grateful for the financial support provided through the GWU Academic Excellence Fellowship.
This work is based upon research conducted at the Cornell High Energy Synchrotron Source which is supported by the National Science Foundation and the National Institutes of Health/National Institute of General Medical Sciences under NSF award DMR-1332208.
Use of the National Synchrotron Light Source, Brookhaven National Laboratory, was supported by the U.S. Department of Energy, Office of Science, Office of Basic Energy Sciences, under Contract No. DE-AC02-98CH10886.
This work benefited from \href{http://ccpsas.org/}{CCP-SAS} software developed through a joint EPSRC (EP/K039121/1) and NSF (CHE-1265821) grant.

\doublespacing
\newpage
%\section{\protect \centering Abstract of Dissertation}
\section{Abstract of Dissertation}
\begin{center}
{\singlespacing \LARGE Dynamic Conformations of Nucleosome Arrays in Solution from Small-Angle X-ray Scattering}
\end{center}
Chromatin conformation and dynamics remains unsolved despite the critical role of the chromatin in fundamental genetic functions such as transcription, replication, and repair. 
At the molecular level, chromatin can be viewed as a linear array of nucleosomes, each consisting of 147 base pairs (bp) of double-stranded DNA (dsDNA) wrapped around a protein core and connected by 10 to 90 bp of linker dsDNA. 

Using small-angle X-ray scattering (SAXS), we investigated how the conformations of model nucleosome arrays in solution are modulated by ionic condition as well as the effect of linker histone proteins. 
To facilitate ensemble modeling of these SAXS measurements, we developed a simulation method that treats coarse-grained DNA as a Markov chain, then explores possible DNA conformations using Metropolis Monte Carlo (MC) sampling.
This algorithm extends the functionality of SASSIE, a program used to model intrinsically disordered biological molecules, adding to the previous methods for simulating protein, carbohydrates, and single-stranded DNA.
Our SAXS measurements of various nucleosome arrays together with the MC generated models provide valuable solution structure information identifying specific differences from the structure of crystallized arrays.

%With the added functionality to simulate DNA, users will be able to , by varying protein, carbohydrate, and single-stranded DNA.
%Additionally, the added capability of SASSIE provides a valuable tool for generating atomistic molecular structures of DNA and DNA-protein systems based on experimental scattering measurements.

\doublespacing
\newpage
\phantomsection \label{toc}
\addcontentsline{toc}{section}{Table of Contents}
\tableofcontents

\newpage
\cleardoublepage
\phantomsection \label{listoffig}
\addcontentsline{toc}{section}{List of Figures}
\begin{center}
\listoffigures
\end{center}

\newpage
\cleardoublepage
\phantomsection \label{listoftab}
\addcontentsline{toc}{section}{List of Tables}
\begin{center}
\listoftables
\end{center}

\newpage
\section{List of Symbols}
\doublespacing
%\setlist[itemize]{font=\normalfont,leftmargin=*}
\begin{itemize}[label={},leftmargin=*]
  \addtolength{\itemindent}{-0.08in}
  % \item[1]{$E$} \hspace{0.5in} beam energy % original formatting
\item{\makebox[12mm][l]{$\alpha$}} DNA dihedral angle formed by the atoms: O3$'$-P-O5$'$-C5$'$
\item{\makebox[12mm][l]{$\beta$}} DNA dihedral angle between the atoms: P-O5$'$-C5$'$-C4$'$
\item{\makebox[12mm][l]{$\beta(Q)$}} measure of the level of polydispersity and spherical symmetry of a particle
\item{\makebox[12mm][l]{$c$}} speed of light
\item{\makebox[12mm][l]{$c_\text{scale}$}} scale constant used to scale a model and experimental data set
\item{\makebox[12mm][l]{$\gamma$}} DNA dihedral angle between the atoms: O5$'$-C5$'$-C4$'$-C3$'$
\item{\makebox[12mm][l]{$\Gamma$}} gamma function
\item{\makebox[12mm][l]{$\delta$}} DNA dihedral angle between the atoms: C5$'$-C4$'$-C3$'$-O3$'$
\item{\makebox[12mm][l]{$D_\text{max}$}} maximum distance between atoms in a particle
\item{\makebox[12mm][l]{$D_\text{order}$}} length of ordering
\item{\makebox[12mm][l]{$D_\text{repeat}$}} distance between repeated structure features
\item{\makebox[12mm][l]{$\epsilon$}} dielectric constant of water
\item{\makebox[12mm][l]{$\varepsilon$}} DNA dihedral angle between the atoms: C4$'$-C3$'$-O3$'$-P
\item{\makebox[12mm][l]{$E$}} beam energy
\item{\makebox[12mm][l]{$F$}} $F$-factor (fitness function)
\item{\makebox[12mm][l]{$\mathcal{F}$}} Fourier transform
\item{\makebox[12mm][l]{$\mathcal{F}^{-1}$}} reverse Fourier transform
\item{\makebox[12mm][l]{$h$}} Plank constant
\item{\makebox[12mm][l]{$I_e$}} experimental scattering intensity
\item{\makebox[12mm][l]{$I_j$}} scattering intensity of pixel $j$
\item{\makebox[12mm][l]{$I_m$}} theoretical scattering intensity of a model structure
\item{\makebox[12mm][l]{$I(Q)$}} scattering intensity as a function of momentum transfer
\item{\makebox[12mm][l]{$I'(Q)$}} $I(Q)$ transformed using the Fourier transform then transformed back with the reverse Fourier transform
\item{\makebox[12mm][l]{$I_{Q_\text{bin}}$}} scattering intensity averaged from all pixels in a specific $Q_\text{bin}$
\item{\makebox[12mm][l]{$\kappa$}} harmonic twist force constant between DNA bp
\item{\makebox[12mm][l]{$\kappa_\text{attr}$}} inverse Debye screening length for attraction
\item{\makebox[12mm][l]{$k_B$}} Boltzmann constant
\item{\makebox[12mm][l]{$\kappa_D$}} inverse Debye screening length for repulsion
\item{\makebox[12mm][l]{$l$}} length of inextensible rod connecting beads in discrete wormlike chain
\item{\makebox[12mm][l]{$L$}} total contour length of a wormlike chain
\item{\makebox[12mm][l]{$\lambda$}} X-ray or neutron beam wavelength
\item{\makebox[12mm][l]{$L_p$}} persistence length of DNA
\item{\makebox[12mm][l]{$n$}} number of degrees of freedom
\item{\makebox[12mm][l]{$N_{e}$}} number of repeated exposures
\item{\makebox[12mm][l]{$N_{g}$}} number of grid points used in the reciprocal space convergence
\item{\makebox[12mm][l]{$n_p$}} average number density of particles
\item{\makebox[12mm][l]{$N_{Q}$}} number of $Q$-points
\item{\makebox[12mm][l]{$N_{Q_\text{bin}}$}} number of pixel in a specific $Q_\text{bin}$
\item{\makebox[12mm][l]{$\rho$}} twist angle of an NCP about its dyad axis
\item{\makebox[12mm][l]{$P_n$}} probability of obtaining an observed $\chi^2$ given $n$ degrees of freedom
\item{\makebox[12mm][l]{$\varphi$}} slide angle used to slide stacked NCPs perpendicular to their stack axis
\item{\makebox[12mm][l]{$P(Q)$}} single-particle form factor as a function of momentum transfer
\item{\makebox[12mm][l]{$P(r)$}} average pair distance distribution function
\item{\makebox[12mm][l]{$ps$}} picosecond
\item{\makebox[12mm][l]{$\psi$}} opening angle between the center-to-center segments of three connected nucleosomes
\item{\makebox[12mm][l]{$Q$}} momentum transfer in elastic scattering event
\item{\makebox[12mm][l]{$Q_j$}} momentum transfer of pixel $j$
\item{\makebox[12mm][l]{$r$}} distance between atoms
\item{\makebox[12mm][l]{$R$}} $R$-factor (residual factor)
\item{\makebox[12mm][l]{$R_e$}} end-to-end distance
\item{\makebox[12mm][l]{$R_g$}} radius of gyration
\item{\makebox[12mm][l]{$R_w$}} weighted $R$-factor (residual factor)
\item{\makebox[12mm][l]{\bm$s$}} path function
\item{\makebox[12mm][l]{$S(Q)$}} interparticle structure factor as a function of momentum transfer
\item{\makebox[12mm][l]{$S'(Q)$}} interparticle effective structure factor as a function of momentum transfer
\item{\makebox[12mm][l]{$\sigma$}} width of a discrete wormlike chain
\item{\makebox[12mm][l]{$\sigma_j$}} error in the scattering intensity of pixel $j$
\item{\makebox[12mm][l]{$\sigma_{\!\!_{N}}$}} equivalent NCP diameter
\item{\makebox[12mm][l]{$\sigma_\text{peak}$}} full width at half maximum for a Bragg peak
\item{\makebox[12mm][l]{$\sigma(Q)$}} error in the scattering intensity as a function of momentum transfer
\item{\makebox[12mm][l]{$\sigma_{\!\!_{Q_\text{bin}}}$}} error in the scattering intensity averaged from all pixels in a specific $Q_\text{bin}$
\item{\makebox[12mm][l]{$\sigma_{\!_\text{SD}}$}} standard deviation
\item{\makebox[12mm][l]{$T$}} temperature
\item{\makebox[12mm][l]{$\vartheta$}} half of the scattering angle
\item{\makebox[12mm][l]{$\delta\vartheta_\text{max}$}} maximum sampling angle for DNA MC steps
\item{\makebox[12mm][l]{\bm$u$}} unit vector between continuous segments of a wormlike chain
\item{\makebox[12mm][l]{$U_\text{bend}$}} bending energy of a wormlike chain
\item{\makebox[12mm][l]{$U_\text{HS}$}} hard-sphere potential
\item{\makebox[12mm][l]{\bm$u_k$}} unit vector of the $k^{th}$ rod in a discrete wormlike chain
\item{\makebox[12mm][l]{$U(r)$}} inter-NCP interaction potential as function of inter-NCP distance
\item{\makebox[12mm][l]{$U_\text{WCA}$}} Weeks--Chandler--Andersen form of a purely repulsive Lennard-Jones potential
\item{\makebox[12mm][l]{$\omega$}} twist angle between DNA bp
\item{\makebox[12mm][l]{$w_i$}} weight factor used in merging different concentration data
\item{\makebox[12mm][l]{$\chi$}} DNA dihedral angle between the atoms: O4$'$-C1$'$-N1/N9-C4/C2
\item{\makebox[12mm][l]{$\chi^2$}} chi-squared statistic
\item{\makebox[12mm][l]{$Z_\text{attr}$}} effective NCP attractive charge
\item{\makebox[12mm][l]{$Z_\text{bare}$}} bare NCP charge
\item{\makebox[12mm][l]{$Z_\text{eff}$}} effective NCP repulsive charge
\item{\makebox[12mm][l]{$Z_\text{ren}$}} renormalized effective NCP charge
\item{\makebox[12mm][l]{$\zeta$}} DNA dihedral angle between the atoms: C3$'$-O3$'$-P-O5$'$
\end{itemize}

\newpage
\section{Glossary of Terms and Abbreviations}
\doublespacing
\setlist[description]{font=\normalfont,leftmargin=*}
\begin{description}
%  \addtolength{\itemindent}{0.35in}
\item[1D:] one-dimensional
\item[1KX5:] protein data bank identifier for the atomistic structure model of the crystal structure of the nucleosome core particle \citep{davey_solvent_2002}
\item[1ZBB:] protein data bank identifier for the atomistic structure model of the crystal structure of the tetranucleosome \citep{schalch_xray_2005}
\item[2D:] two-dimensional
\item[3D:] three-dimensional
\item[\AA:] Angstrom, $10^{-10}$ meter
\item[a.a.:] amino acid
\item[API:] application programming interface
\item[array center:] the midpoint between the centers of the four nucleosomes in a tetranucleosome array
\item[BI DNA:] B-form DNA base for which $\epsilon-\zeta>0$
\item[BII DNA:] B-form DNA base for which $\epsilon-\zeta<0$
\item[bp:] DNA base pair
\item[$^\circ$C:] degrees Celsius
\item[Ca$^{2+}$:] calcium cation
\item[CG:] coarse-grain
\item[cm:] centimeter, $10^{-2}$ meter 
\item[CHESS:] Cornell High Energy Synchrotron Source
\item[C-ter:] carboxyl-terminus, or C-terminal, end of a protein chain 
\item[cryo-EM:] cryogenic electron microscopy
\item[CRYSOL:] a program to calculate theoretical X-ray solution scattering of biological macromolecules from atomic coordinates
\item[Da:] Dalton, equal to one atomic mass unit, the mass of a hydrogen atom.
\item[DAMMIN:] a program to restore ab initio low resolution shape of randomly oriented particles in solution
\item[DH:] Debye-H\"uckel
\item[DNA:] deoxyribonucleic acid
\item[dsDNA:] double-stranded deoxyribonucleic acid
\item[{\it e}:] elementary charge, or electric charge carried by a single proton, \SI{1.60217662d-19}{coulombs}
\item[{\it E. coli}:] {\it Escherichia coli} bacteria
\item[FWHM:] full width at half maximum
\item[G1/CHESS:] G1 beamline at the Cornell High Energy Synchrotron Source
\item[gH1:] globular variant of histone H1
\item[gH3:] globular variant of histone H3
\item[gH4:] globular variant of histone H4
\item[gH5:] globular variant of histone H5
\item[GOCM:] generalized one component method
\item[H1:] nucleosome linker histone 
\item[H2A:] nucleosome core histone 
\item[H2B:] nucleosome core histone 
\item[H3:] nucleosome core histone 
\item[H4:] nucleosome core histone
\item[H5:] nucleosome linker histone
\item[HS:] hard-sphere
\item[K$^{+}$:] potassium cation
\item[kDa:] kilodalton, $10^3$ Dalton
\item[keV:] kiloelectron volt, $10^3$ electron volt, unit of energy
\item[km:] kilometer, $10^3$ meter
\item[LB:] lysogeny broth
\item[m:] meter, unit of distance
\item[M:] moles per liter, molar concentration or molarity
\item[MC:] Monte Carlo
\item[MD:] molecular dynamics
\item[MDa:] megadalton, $10^6$ Dalton
\item[mg:] milligrams, unit of mass
\item[Mg$^{2+}$:] magnesium cation
\item[min:] minute
\item[mL:] milliliters, unit of volume
\item[$\mu$m:] micrometer, $10^{-6}$ meter
\item[mm:] millimeter, $10^{-3}$ meter 
\item[mM:] millimolar, 10$^{-3}$ moles per liter
\item[MMTV:] mouse mammary tumor virus, the MMTV DNA sequence acts as a nucleosome positioning sequence but with a lower binding affinity compared to the W601 sequence
\item[mononucleosome:] a single nucleosome
\item[N-ter:] amino-terminus, or N-terminal, end of a protein chain
\item[NAMD:] nanoscale molecular dynamics, a molecular dynamics simulation program \citep{phillips_scalable_2005}
\item[NCP:] nucleosome core particle, a nucleosome with only the \textapprox147 bp of dsDNA wrapping the histone protein core (when we refer to an NCP as part of a nucleosome array, we are referring to a nucleosome separate to any linker DNA)
\item[NCP center:] the intersection of the NCP cylinder axis and the NCP dyad axis
\item[NCP cylinder axis:] the center axis of a cylinder fit to the C1$'$ atoms of an NCP
\item[NCP dyad:] the central DNA bp of an NCP (the 74$^{th}$ bp of a fully wrapped NCP)
\item[NCP dyad axis:] the axis orthogonal to the NCP cylinder axis that passes through the center of the NCP dyad
\item[NCP face:] treating an NCP as a cylinder, the flat surfaces would be the face
\item[NCP side:] treating an NCP as a cylinder, the round surface would be the side
\item[NCP$_i$:] the $i^{th}$ NCP in an nucleosome array; NCP$_1$ is the nucleosome closest to the 5$'$ end of the template DNA
\item[NG-3:] SANS beamline at the National Institute of Standards and Technology Center for Neutron Research
\item[nm:] nanometer, $10^{-9}$ meter
\item[NMR:] nuclear magnetic resonance imaging
\item[NS-NCP:] natural-source NCP, these NCPs were extracted from chicken erythrocytes
\item[NSLS:] National Synchrotron Light Source, Brookhaven National Laboratory
\item[nucleosome:] molecular building block of chromatin, DNA-protein complex containing 2 copies each of core histones H2A, H2B, H3, and H4, wrapped \textapprox1.65 times by dsDNA.  The difference between a nucleosome and a nucleosome core particle is that the nucleosome contains more that the minimum \textapprox147 bp dsDNA.
\item[{\it N}$\times$167:] notation for referring to a nucleosome array with $N$ repeats of the W601 nucleosome positioning sequence connected by 20 bp of linker dsDNA
\item[opening angle:] the angle between the center-to-center segments of three connected nucleosomes
\item[PAGE:] polyacrylamide gel electrophoresis 
\item[PDB:] \href{http://www.rcsb.org/pdb/home/home.do}{Protein Data Bank} file containing the coordinates of a molecular structure
\item[PSF:] Protein Structure File containing the bond information for a molecular structure
\item[PSFGEN:] PSF generator, a program to generate a PDB and PSF pair used to fully define a molecular structure's coordinates and bond information respectively
\item[RC-NCP:] recombinant NCP
\item[rpm:] revolutions per minute
\item[{[salt]}:] Square brackets around a quantity is a common mechanism to denote the concentration of that quantity, often used for referring to a salt or mass concentrations.  In this example, [salt] represents the salt concentration.
\item[SANS:] small-angle neutron scattering
\item[SAS:] small-angle scattering
\item[SASSIE:] small angle scattering systematic integrated environment, a program developed at the NIST Center for Neutron Research to model intrinsically disordered biological molecules
\item[SAXS:] small-angle X-ray scattering
\item[SDS:] sodium dodecyl sulfate 
\item[SNR:] signal-to-noise ratio
\item[ssNA:] single-stranded nucleic acid
\item[stack axis:] the line pointing from the center of NCP$_i$ to the center of NCP$_{i+2}$
\item[tetranucleosome:] an array of four connected nucleosomes 
\item[twist angle:] the dihedral angle between NCP$_i$ and NCP$_{i+1}$, formed by the center-to-center segment between NCPs and the two cylinder axes
\item[UV:] ultraviolet
\item[V:] volt, electric potential
\item[W601:] Widom 601 DNA sequence \citep{lowary_new_1998} know to strongly bind and therefore position nucleosomes 
\item[WAXS:] wide-angle X-ray scattering
\item[WCA:] Weeks--Chandler--Andersen
\item[X9/NSLS:] X9 beamline at the National Synchrotron Light Source, Brookhaven National Laboratory
\end{description}
 
\newpage
\vspace*{\fill}
%\quad\\
\begin{center}
  \textit{``If our small minds, for some convenience, divide this \dots universe, into parts---physics, biology, geology, astronomy, psychology, and so on---remember that nature does not know it!''}\\
\end{center}
\textit{\hspace{7cm} ---Richard P. Feynman, 1963}\\
% \textit{\hspace{5cm} \, {\it The Feynman Lectures on Physics}, lec.3, 10, 1963}\\
\vspace{1in}
\begin{center}
  \textit{``Focus too closely on the goal you haven't accomplished, and you'll fail to notice the victories you achieve along the way.''}\\
\end{center}
\textit{\hspace{7cm} ---Orson Scott Card, ``Earth Awakens''}\\
\quad\\
\vspace*{\fill}
\setcounter{page}{1}
\pagenumbering{arabic}
