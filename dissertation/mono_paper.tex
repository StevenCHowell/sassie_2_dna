% $Id: mononucleosome.tex 103 2015-09-22 21:18:05Z schowell $
\newpage
\begin{singlespace}
\section{Chapter 2: Structure and Interaction of Nucleosome Monomers}\label{chp:mono}%Quantify inter-nucleosome forces}
\end{singlespace}
\doublespacing
\noindent 
%--------------------------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%% NUCLEOSOMES %%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Chapter Overview}\label{sub:mono:overview}%Quantify inter-nucleosome forces}
The first step in exploring the solution structure of nucleosome arrays is to gain a clear understanding of the structure and interaction of NCPs in solution.
This understanding provides the foundation for surveying more complex arrays of nucleosome.

The following peer reviewed publication documents our study of natural-source NCPs (NS-NCP), and two NCP constructs with epigenetic modifications.
By varying the NCP and ion concentrations, abbreviated as [NCP] and [ion], we quantify the inter-NCP pair potentials.
We also compare the difference interaction effects caused by H3 versus H4 histone modifications.
% Additionally, these efforts demonstrate our ability to prepare and measure nucleosomes solutions to obtain high quality SAXS data and also our ability to reduce and analyze this data.
  
\subsection{Elucidating Internucleosome Interactions and the Roles of Histone Tails}
Steven C.~Howell,$^\dagger$ Kurt Andresen,$^\ddagger$ Isabel Jimenez-Useche,$^\S$, Chongli Yuan, $^\S$ and Xiangyun Qiu$^\dagger$

\noindent
$^\dagger$Department of Physics, George Washington University, Washington, DC;\\
$^\ddagger$Department of Physics, Gettysburg College, Gettysburg, Pennsylvania;\\
$^\S$School of Chemical Engineering, Purdue University, West Lafayette, Indiana

\noindent
Biophysical Journal\quad Volume 105\quad July 2013\quad 194-199

\noindent
DOI: \href{http://linkinghub.elsevier.com/retrieve/pii/S0006349513005778}{10.1016/j.bpj.2013.05.021}

\subsubsection{Abstract}\label{sub:mono:abstract}
The nucleosome is the first level of genome organization and regulation in eukaryotes where negatively charged DNA is wrapped around largely positively charged histone proteins. 
Interaction between nucleosomes is dominated by electrostatics at long range and guided by specific contacts at short range, particularly involving their flexible histone tails. 
We have thus quantified how internucleosome interactions are modulated by salts (KCl, MgCl$_2$) and histone tail deletions (H3, H4 N-terminal), using SAXS and theoretical modeling. 
We found that measured effective charges at low salts are \textapprox1/5$^{th}$ of the theoretically predicted renormalized charges and that H4 tail deletion suppresses the attraction at high salts to a larger extent than H3 tail deletion.

\subsubsection{Introduction}\label{sub:mono:intro}
Highly charged DNA is packaged into NCP in eukaryotic cells by wrapping every 147 per \textapprox180 bp of DNA around a histone octamer. 
The octamer is a protein complex comprising two copies of each of the four histone proteins (named H2A, H2B, H3, and H4). 
Each histone has one C-terminal and one N-terminal tail that protrude out from the octameric core. 
Linked by \textapprox20--60 bp DNA like beads on a string, NCPs are further packaged into chromatin whose conformation and dynamics underpins gene maintenance and regulation. 
Thus, there has been long-standing interest in physically understanding nucleosome structure and interaction fundamental to chromatin function.

Polyelectrolyte behavior and histone tail modulation are among the key determinants of the multifactorial packaging of nucleosomes \citep{hansen_conformational_2002}. 
One NCP carries a net negative charge of \textapprox\SI{150}{\textit{e}} with $-\SI{294}{\textit{e}}$ from DNA and $+\SI{144}{\text{\textit{e}}}$ from histone proteins. 
Given its diameter of \textapprox\SI{10}{nm} and height of \textapprox\SI{6}{nm}, this leads to an average charge density of $-\SI{0.43}{\text{\textit{e}/nm$^2$}}$, about half of the $-\SI{1}{\text{\textit{e}/nm$^2$}}$ of DNA. 
The expected polyelectrolyte behavior of NCP is demonstrated by cation-modulated unfolding and compaction of NCP arrays \citep{clark_electrostatic_1990,korolev_electrostatic_2010}.
Notably, divalent cations go beyond screening the electrostatic repulsion between NCPs and can mediate inter-NCP attraction, e.g., Mg$^{2+}$ or Ca$^{2+}$ induced condensation of mono- and poly-nucleosomes \citep{defrutos_aggregation_2001}. 
In contrast, these divalent cations are unable to condense DNA. 
This suggests significant roles of the complex charge distribution of NCP furnished by the histones \citep{sun_electrostatic_2005}. 
In particular, the tails of histones (\textapprox30\% of the histone mass), being highly positively charged and flexible, are known to be crucial in modulating nucleosome interaction and chromatin assembly \citep{hansen_conformational_2002}. 

There have been extensive experimental studies aiming to elucidate the electrostatics of nucleosomes and the roles of histone tails. 
The effects of cations of varied valences on the nucleosome assembly have been well characterized \citep{hansen_conformational_2002,clark_electrostatic_1990,korolev_electrostatic_2010,widom_structure_1998}, e.g., by measuring the sedimentation coefficients of nucleosome arrays or the fraction of condensed nucleosomes \citep{defrutos_aggregation_2001, dorigo_chromatin_2003}.
Among the 16 histone tails, the eight N-terminal tails hold the most positive charges and are considered more significant, e.g., NCPs with all N-terminal tails deleted remain soluble in high divalent salts \citep{gordon_core_2005}. 
In an effort to dissect the contribution from each N-terminal tail, all 16 possible tail-deletion NCP mutants have been studied and it was found that all N-terminal tails contribute to the condensation of oligo-NCP arrays nonspecifically and additively \citep{gordon_core_2005}.
Still, the histone tails vary in length and charge and thus differ in strength in mediating nucleosome interactions, e.g., H3 and H4 tails, studied herein, were observed to be more important than H2A and H2B tails \citep{gordon_core_2005,allahverdi_effects_2011}.

Systematic measurements have promoted widespread theoretical interests in mechanistically understanding the structure and interaction of nucleosomes \citep{schlick_convergence_2012,korolev_modelling_2012}. 
At the atomic level, enabled by the availability of high resolution crystal structures \citep{richmond_structure_2003}, molecular dynamics simulations have described the ion and solvent atmospheres of nucleosome and revealed rugged electrostatic surfaces \citep{gan_chromatin_2010, materese_counterion_2009}.
The flexible histone tails have been analyzed in detail and shown to display multistate conformational dynamics \citep{potoyan_energy_2011,arya_tale_2009,muhlbacher_tailinduced_2006}.
At the CG level, both nucleosome arrays and chromatin have been modeled to recapture the conformational changes under varied conditions such as salt and mechanical stretching \citep{diesinger_depletion_2009,kulic_chromatin_2003}.
However, to apply the detailed NCP structures and energetics predicted by theoretical approaches, stringent experimental tests are required, but largely lacking. 
For example, as the focus of this study, one key factor to the assembly of NCPs is the effective inter-NCP potential that is usually computed following the Poisson-Boltzmann formalism for efficiency \citep{korolev_modelling_2012}.
However, such theoretical treatment has not been validated experimentally due to the lack of direct measurements of inter-NCP potentials. 
Comparisons with previously measured quantities are often difficult, e.g., the interpretation of sedimentation coefficient necessitates an ab initio hydrodynamic model.

Here, we report measurements of interactions between monomeric nucleosomes using solution SAXS in conjunction with theories of polyelectrolytes. 
There are two outcomes we deem important: 1), Quantification of inter-NCP pair potentials by modeling full SAXS profiles; 2), Elucidation of the individual role of H3 and H4 tails. 
Although SAXS (and small-angle neutron scattering for neutron) has been used to study the structure and interaction of NCPs since the 1970s \citep{hjelm_small_1977}, no quantitative refinement of full SAXS profiles has been done to extract the inter-NCP interactions, which we obtain here by refining synchrotron SAXS data. 
One key advantage of full profile modeling is quantification of the inter-NCP potential as a function of inter-NCP distance \citep{chen_smallangle_1988}, in contrast to single-parameter measurements such as second virial coefficients or osmotic pressures of NCP solutions. 
Bertin et al.~\citep{bertin_h3_2007} have compared full SAXS profiles with theoretical calculations. 
However, refinements were not performed, possibly due to the relatively dilute NCP concentrations ($\leq \SI{8}{mg/mL}$, \textapprox\SI{0.04}{mM}) and limited angular range of the in-house SAXS data that lower SAXS sensitivity to inter-NCP interactions. 
In addition, using recombinant NCP constructs with either H3 or H4 tail deleted, to the best of our knowledge we report the first analysis of this kind on the respective role of H3 and H4 tails in mediating inter-NCP interactions.

\subsubsection{Material and Methods}
Monomeric NS-NCPs were prepared from adult chicken erythrocytes as in \citep{yager_dynamics_1984}. 
Gel electrophoresis was used to assess the DNA length ($147\pm\SI{5}{bp}$) and the histone content (\cref{fig:mono:s1}, (i) and (ii), in \hyperref[app:mono]{appendix A}). 
Recombinant nucleosomes (RC-NCP) were reconstituted by mixing 147 bp mouse mammary tumor virus (MMTV) DNA and histone octamers with sequences from \textit{Xenopus laevis} following standard protocols \citep{gordon_core_2005} (\cref{fig:mono:s1}, (i) and (iii)). 
The globular H3 and H4 recombinant proteins were truncated at their N-terminal ends (referred to as gH3 and gH4 RC-NCPs), by deleting the coding sequences of amino acids (1--27 with $+\SI{10}{\textit{e}}$ of H3 and 1--10 with $+\SI{3}{\textit{e}}$ of H4) via site-directed mutagenesis.
All NCP stocks were purified by size-exclusion chromatography (Sephacryl S300-HR medium) and then dialyzed against \SI{10}{mM} KCl \SI{10}{mM} Tris \SI{0.1}{mM} EDTA pH 7.5 buffer. 
Salts were adjusted to studied conditions by adding concentrated salts. 
SAXS experiments were carried out at \SI{20}{$^\circ$C} at the G1 station at the Cornell High Energy Synchrotron Source in Ithaca, New York. 
The incident beam had an energy of \SI{9.97}{keV} and size of $250\times\SI{250}{$\mu$m$^2$}$.
Samples of \textapprox\SI{30}{$\mu$L} were injected into an in-vacuum capillary flow cell to enable windowless data collection for background reduction. 
Radiation damage was mitigated by optimizing X-ray exposure time and oscillating the plug of sample solution. 
Six to eight two-second exposures of the same sample were taken and verified to be reproducible, indicative of the absence of radiation damage.

\subsubsection{Results}\label{sec:mono:results}
\begin{figure}[!b]
  \centerline{\includegraphics[width=0.6\textwidth]{figures/mono/fig1}}
  \caption[NS-NCP SAXS Form Factor]{\label{fig:mono:1} 
    \textbf{SAXS Form Factor:}
    $P(Q)$ of NS-NCP from experiment ({\tiny{\color{blue}$\bm{\square}$}}) and crystal structure ({\color{red}\textit{red}}, PDB ID: 1KX5). 
    %$Q = 4\pi sin(\vartheta)/\lambda$ is the scattering vector, where $2\vartheta$ is the scattering angle and $\lambda$ is the X-ray wavelength. 
    The experiment form factor was measured at a dilute NCP concentration of \SI{0.003}{mM} (\textapprox\SI{0.6}{mg/mL}) and an intermediate salt (\SI{40}{mM} KCl) to minimize the influence of inter-NCP interferences. 
    Error bars of $I(Q)$s, in this and subsequent plots, are smaller than symbol sizes except at high $Q$s. 
    Inset shows the Guinier fit (solid line) to obtain the radius of gyration ($R_g$). 
    The linear region extends well over the empirical $Q_\text{max}$ cutoff of $1.3/R_g$ for globular structures.
  }
\end{figure} 
Measured SAXS profile $I(Q)$ is a product of the form factor $P(Q)$ and the structure factor $S(Q)$, i.e., $I(Q)=P(Q)\times S(Q)$,
with $S(Q)$ becoming significant under conditions such as high [NCP]s and strong inter-NCP interactions. 
$I(Q)$ of a dilute NCP solution (i.e., $S(Q)=1$) thus measures the structure of individual NCPs in terms of the form factor $P(Q)=I(Q)$. 
This can reveal differences between molecular structures in solution and in crystal. 
Fig.~\ref{fig:mono:1} shows excellent agreement between the SAXS profile measured in solution and the profile calculated from a crystal structure in $Q$ range of 0.01--\SI{0.28}{\angstrom$^{-1}$}, indicative of high sample purity and structural conformity. 
The same level of agreement was observed for all NCP constructs (i.e., NS-NCP, wild-type, gH3, and gH4 RC-NCPs). 
It is worth noting that the crystal structure $P(Q)$ shows an extra dip around $Q$ of \SI{0.14}{\angstrom$^{-1}$}.
This discrepancy has been observed in previous reports \citep{bertin_h3_2007,yang_biophysical_2011} and was attributed to the DNA unwrapping at the ends by a few base pairs. 
Tail deletions lead to small changes of the dip around $Q = \text{\SI{0.14}{\angstrom^{-1}}}$ that can be attributed to increased DNA unwrapping (data not shown). 
Nonetheless, these dips appear in relatively high $Q$ and do not affect our subsequent analysis. 
Guinier analysis of the low $Q$ data (\cref{fig:mono:1}, inset) gives the radius of gyration $R_g$ = \SI{44.4 (3)}{\angstrom}, which is nearly identical to previous SAXS measurements of natural-source NCPs \citep{mangenot_saltinduced_2002} and recombinant NCPs with $\alpha$-satellite DNA \citep{yang_biophysical_2011}. 
Note that NCPs reconstituted with the 601 sequence were reported to give slightly smaller $R_g$ by 2--\SI{3}{\angstrom} \citep{bertin_h3_2007,yang_biophysical_2011}.

\begin{figure}[!b]
  \centerline{\includegraphics[width=\textwidth]{figures/mono/fig2}}
  \caption[SAXS Profiles and GOCM Fits of NS-NCP in a Series of KCl Concentrations]{\label{fig:mono:2} 
    \textbf{SAXS Profiles, \bm{$I(Q) = P(Q) \times S(Q)$}, of NS-NCPs at Four KCl Concentrations:}
    I(Q)s are normalized by NCP concentrations to assist comparison.
    Each panel shows experimental I(Q)s (\textit{symbols}) at a series of [NCP]s (\SI{0.1}{mM} gives \textapprox\SI{20}{mg/mL}) as indicated in the legends, together with their respective theoretical fits (\textit{lines}). 
    The residues are shown with an offset at the same scale. 
    The pertinent pair-potential parameters are (discussed in detail in the main text), 
    {\bf a}: $Z_\text{eff} =  \SI{22(1)}{\textit{e}}$,  $\sigma_{\!\!_{N}} = \text{\SI{140}{\angstrom}}$; 
    {\bf b}: $Z_\text{eff} =  \SI{23(1)}{\textit{e}}$,  $\sigma_{\!\!_{N}} = \text{\SI{110}{\angstrom}}$; 
    {\bf c}: $Z_\text{attr} = \SI{150(5)}{\textit{e}}$, $\sigma_{\!\!_{N}} = \text{\SI{100}{\angstrom}}$; 
    {\bf d}: $Z_\text{attr} = \SI{200(8)}{\textit{e}}$, $\sigma_{\!\!_{N}} = \text{\SI{100}{\angstrom}}$. 
    Uncertainties of Z values originate from both curve fitting (via variance-covariance matrix, \textapprox1\%) and small variations (\textapprox3\%) between values at different [NCP]s. 
    The $Z_\text{eff}$ and $Z_\text{attr}$ values are poorly defined when [NCP] is small ($<\SI{0.03}{mM}$, or \SI{6}{mg/mL}) and their values given are from data at high [NCP]s.
  }
\end{figure} 

In addition to structure information, SAXS is capable of quantifying intermolecular interactions, via the structure factor $S(Q)$ originating from spatial correlations between molecules. 
Qualitatively, repulsive interaction leads to a decrease of low $Q$ intensities, i.e., a downturn, and attractive interaction gives rise to an increase at low Q, i.e., an upturn.
Fig.~\ref{fig:mono:2} shows how SAXS profiles evolve with NCP and salt concentrations. 
As expected from strong inter-NCP repulsion at low salts (10 and \SI{40}{mM} KCl, \cref{fig:mono:2}, {\bf a} and {\bf b}), low $Q$ downturns are observed and deepen upon increasing [NCP]. 
In contrast, low $Q$ upturns appear and heighten with [NCP] at high salts (100 and \SI{200}{mM} KCl, \cref{fig:mono:2}, (c) and (d)), indicating inter-NCP attractions. 
These observations are consistent with reports of negative virial coefficients at $>\SI{70}{mM}$ KCl \citep{bertin_h3_2007}.
Divalent Mg$^{2+}$, known to condense NCP at $>\SI{2.5}{mM}$ [Mg$^{2+}$] \citep{defrutos_aggregation_2001}, was also studied and the onset of inter-NCP attraction was found to be between 1 and \SI{2}{mM} [Mg$^{2+}$] (\cref{fig:mono:s3}).


\begin{table}[!t]
  \caption[Inter-NCP Interaction Parameters]{\label{tab:mono:Z} 
    \textbf{Inter-NCP Interaction Parameters:}
    Results from structure factor fittings with either one-DH potential (in the main text) or two-DH potential (in \hyperref[app:mono]{appendix A}). 
    As the structure factors are more pronounced at high NCP concentrations, the values in this table are taken as the mean over the 2-3 most concentrated samples for which the fits are the most reliable. 
    For the fits with one DH term in the inter-NCP potential, the fitted effective charge ($Z_\text{eff}$ or $Z_\text{attr}$) shows no systematic trends with the NCP concentration.
    For the fits with two DH terms, the fitted effective charge ($Z_\text{eff}$) generally increases slightly with the increase of the NCP concentration.
    We do not know the reason for the different trends from fitting the same data and speculate that this may arise from how the generalized one component method handles the effects of sample concentration with or without an attractive term \citep{chen_smallangle_1988}. 
    Note the values for the gH4-NCP construct at \SI{40}{mM} KCl are unreliable due to the relative low [NCP]s.
  }
  %\footnotesize
  % \begin{tabular}{| p{1.2cm} | p{1.35cm} p{2cm} p{0.7cm} p{0.8cm} p{1.2cm} p{2cm} p{2cm} p{1.6cm} |}
  \begin{tabular}{ | l | l | l | l | l | l | l | l | }
    \cline{3-8}
    \multicolumn{2}{c|}{}   & \multicolumn{3}{|c|}{One-DH inter-NCP} & \multicolumn{3}{|c|}{Two-DH inter-NCP} \\[-1.8mm]
    \multicolumn{2}{c|}{}   & \multicolumn{3}{|c|}{Potential} & \multicolumn{3}{|c|}{Potential} \\ \hline
    NCP Construct            & [KCl] (mM) & $\sigma_{\!\!_{N}}$ (\AA) & $Z_\text{eff}$ & $Z_\text{attr}$        & $\sigma_{\!\!_{N}}$ (\AA) & $Z_\text{eff}$ & $Z_\text{attr}$ \\ \hline
    \multirow{4}{*}{NS-NCP}  &  10        & 140            & 22(1)        &      0           & 100            &  35(4)       & 200 \\ \cline{2-8}
                             &  40        & 110            & 23(1)        &      0           & 100            &  49(6)       & 200 \\ \cline{2-8}
                             & 100        & 100            &     0        & 150(5)           & 100            &  45(8)       & 200 \\ \cline{2-8}
                             & 200        & 100            &     0        & 200(8)           & 100            &   1(1)       & 200 \\ \hline %\cline{2-8}
    \multirow{4}{*}{gH3-NCP} &  10        & 140            & 21(1)        &      0           & 100            &  31(3)       & 165 \\ \cline{2-8}
                             &  20        & 130            & 22(1)        &      0           & 100            &  38(3)       & 165 \\ \cline{2-8}
                             &  40        & 110            & 21(1)        &      0           & 100            &  48(4)       & 165 \\ \cline{2-8}
                             & 100        & 100            &     0        & 123(4)           & 100            &  23(4)       & 165 \\ \hline %\cline{2-8}
    \multirow{4}{*}{gH4-NCP} &  10        & 140            & 20(1)        &      0           & 100            &  30(3)       & 154 \\ \cline{2-8}
                             &  20        & 130            & 23(1)        &      0           & 100            &  33(3)       & 154 \\ \cline{2-8}
                             &  40        &  --            &    --        &     --           &  --            &    --        &  -- \\ \cline{2-8}
                             & 100        & 100            &     0        & 100(4)           & 100            & 30(10)       & 154 \\ \hline % \cline{2-8}
    RC-NCP                   &  10        & 140            & 21(1)        &      0           & 100            &  34(3)       & 200 \\ \hline
  \end{tabular}
\end{table}
To gain quantitative insights into the underlying inter-NCP interactions, we then applied the generalized one component method (GOCM) with the mean spherical approximation \citep{chen_smallangle_1988, liu_effective_2005} to model the structure factors. 
As both repulsion and attraction exist, the general form of inter-NCP interaction potential as a function of inter-NCP distance is given by
\begin{align}\label{eqn:mono:dh}
  U(r) = \left\{ 
  \begin{array}{l l}
    \infty & \quad r<\sigma_{\!\!_{N}}\\
    % \displaystyle\frac{Z_\text{eff}^2}{\epsilon\left(1+\left.\kappa_D\,\sigma_{\!\!_{N}}\right/2\right)^2}\frac{e^{-\kappa_D\left(r-\sigma_{\!\!_{N}}\right)}}{r}-\frac{Z_\text{attr}^2}{\epsilon\left(1+\left.\kappa_\text{attr}\,\sigma_{\!\!_{N}}\right/2\right)^2}\frac{e^{-\kappa_\text{attr}\left(r-\sigma_{\!\!_{N}}\right)}}{r} & \quad r\geq\sigma_{\!\!_{N}}
    \dfrac{Z_\text{eff}^2}{\epsilon\left(1+\frac{\kappa_D\,\sigma_{\!\!_{N}}}{2}\right)^2}\dfrac{e^{-\kappa_D\left(r-\sigma_{\!\!_{N}}\right)}}{r}-\dfrac{Z_\text{attr}^2}{\epsilon\left(1+\frac{\kappa_\text{attr}\,\sigma_{\!\!_{N}}}{2}\right)^2}\dfrac{e^{-\kappa_\text{attr}\left(r-\sigma_{\!\!_{N}}\right)}}{r} & \quad r\geq\sigma_{\!\!_{N}}
  \end{array} \right.\,,
\end{align}
where $\sigma_{\!\!_{N}}$ is the equivalent NCP diameter, $Z_\text{eff}$ and $Z_\text{attr}$ are the effective charges to be fitted, $\epsilon=80.4$ is the dielectric constant of water at \SI{20}{$^\circ$C}, and $\kappa_D$ is the inverse Debye screening length calculated from the salt condition. 
Namely, the inter-NCP potential comprises a hard-sphere (HS) repulsive term, a Debye-H\"uckel (DH) electrostatic repulsive term, and a DH-like attractive term. 
Although it is possible to include both DH potentials in the fitting procedures, there exist significant correlations between the fitted repulsive and attractive DH terms despite better fits (see \hyperref[app:mono]{appendix A} for detailed results and discussions). 
Due to this complication, the attractive DH potential is turned off (i.e., $Z_\text{attr}=0$) under conditions where a net inter-NCP repulsion is observed. 
Likewise, $Z_\text{eff}$ is set to 0 under conditions where a net attraction is observed. 
As the physical origin of the attraction is poorly understood, we chose a short-range attraction of effective charge $Z_\text{attr}$ and \SI{4.8}{\angstrom} decay length for all data. 
The effect of NCP's disk-like shape (axis ratio \textapprox2) on $S(Q)$ only becomes significant at $Q>\text{\SI{0.036}{\angstrom$^{-1}$}}$ \citep{bertin_h3_2007} where $S(Q)\sim1$ as observed in the experiments; it is thus not considered.
Such GOCM fits are shown in \cref{fig:mono:2} as solid lines, noting that the form factor for each NCP construct is calculated based on the crystal structure (shown in \cref{fig:mono:s2}). 
It is evident that the GOCM fits are in good agreement with experimental data over the entire $Q$ range. 
Inter-NCP interactions are thus quantified in the forms of HS and DH potentials.

Despite being quantitative, directly usable, and comparable with theoretical studies, it should be noted that the sum of HS and DH potentials describes an apparent inter-NCP interaction that reproduces the measured structure factor.
In the case of inter-NCP repulsion, the effective charge $Z_\text{eff}$, as the only fitting parameter, differs from the bare charge $Z_{bare}$ due to the nonlinear nature of ion screening and the validity of DH potentials in linear regime only.
For the same reason, the equivalent diameter $\sigma_{\!\!_{N}}$ encloses the vicinity with a large electrostatic field (e.g., $>3.8\,k_B T/e$ was used for dsDNA by the authors \citep{qiu_measuring_2006}), which is in the order of Debye length. 
Especially at low salt, $\sigma_{\!\!_{N}}$ is significantly larger than the \SI{100}{\angstrom} computed from the NCP crystal structure \citep{chen_smallangle_1988}. 
We determined the value of $\sigma_{\!\!_{N}}$ at different salts based on cell model numerical calculations \citep{qiu_measuring_2006,trizac_alexanders_2003}, and the same $\sigma_{\!\!_{N}}$ value was then fixed for all data at the same ionic condition for consistency. 
We show these relevant interaction potential parameters in the figure captions and in \cref{tab:mono:Z}.

\begin{figure}[!b]
  \centerline{\includegraphics[width=\textwidth]{figures/mono/fig3}}
  \caption[{SAXS Profiles and GOCM Fits of gH3 and gH4 RC-NCPs in 10 and \SI{100}{mM} KCl}]{\label{fig:mono:3} 
    \textbf{SAXS Profiles, $I(Q) = P(Q) \times S(Q)$, of gH3 and gH4 RC-NCPs at 10 and 100 mM KCl:}
    Annotations follow that of \cref{fig:mono:2}. 
    Inter-NCP potential parameters are, 
    {\bf a}: $Z_\text{eff}  = \SI{21(1)}{\textit{e}}$,  $\sigma_{\!\!_{N}} = \text{\SI{140}{\angstrom}}$; 
    {\bf b}: $Z_\text{attr} = \SI{123(4)}{\textit{e}}$, $\sigma_{\!\!_{N}} = \text{\SI{100}{\angstrom}}$; 
    {\bf c}: $Z_\text{eff}  = \SI{20(1)}{\textit{e}}$,  $\sigma_{\!\!_{N}} = \text{\SI{140}{\angstrom}}$; 
    {\bf d}: $Z_\text{attr} = \SI{100(4)}{\textit{e}}$, $\sigma_{\!\!_{N}} = \text{\SI{100}{\angstrom}}$. 
    It is worth noting that, although gH4 RC-NCP shows no interaction at \SI{100}{mM} KCl, a significant $Z_\text{attr}$ exists so as to negate the HS repulsion with $\sigma_{\!\!_{N}} = \text{\SI{100}{\angstrom}}$, which makes $Z_\text{attr}$ values artificially large.
    $Z_\text{attr}$ would be zero if $\sigma_{\!\!_{N}}$ is zero for gH4 RC-NCP in \SI{100}{mM} KCl. 
    It is thus more appropriate to interpret $Z_\text{attr} = 100 $ as contributed by the inter-NCP short-range attraction.
  }
\end{figure} 
Figure \ref{fig:mono:3} shows data from recombinant gH3 and gH4 RC-NCPs at two salt conditions (10 and \SI{100}{mM} KCl) together with GOCM fits (data at 20 and \SI{40}{mM} KCl are shown in \cref{fig:mono:s4}). 
We primarily focused on gH3 and gH4 RC-NCPs because recombinant samples were relatively limited in volume and previous studies have reported the second virial coefficients (though no inter-NCP potentials) of intact RC-NCPs and gH3gH4 RC-NCPs, i.e., with none
or both tails deleted \citep{bertin_h3_2007}. 
To enable cross-comparisons, intact RC-NCPs were measured at a few selected conditions (\cref{fig:mono:s5}). 
At low salt of \SI{10}{mM} KCl, essentially no difference in the effective charge $Z_\text{eff}$ (\textapprox\SI{22}{\textit{e}}) was observed among all NCP constructs (i.e., NS-NCP, gH3, gH4, and intact RC-NCPs), noting that the negativity of $Z_\text{eff}$ is omitted for brevity. 
We further calculated the second virial coefficient from the inter-NCP potential using
\begin{align}
A_2 = \frac{1}{2}\int_0^\infty\!\left(1-e^{U(r)/k_BT}\right)4\pi \,r^2\,dr\,,
\end{align}
and obtained a value of \SI{2.5(1)d-4}{\text{mL$\cdot$mol/g$^2$}}. 
This value is in excellent agreement with the osmometry measurement by Mangenot et al.~\citep{mangenot_interactions_2002}. 
Bertin et al.~\citep{bertin_h3_2007} used theoretical calculations of $Z_\text{eff}$ between 70 and \SI{125}{\textit{e}} and obtained much larger A$_2$ values close to \SI{5.0d-4}{\text{mL$\cdot$mol/g$^{2}$}}. 
We attribute the difference to the discrepancy between theoretical and experimental $Z_\text{eff}$ values (discussed below) used by Bertin et al.~and us, respectively. 
At high salt of \SI{100}{mM} KCl, qualitative differences were observed: NS-NCP, gH3 and intact RC-NCPs show comparable levels of attraction; whereas gH4 and gH3gH4 RC-NCPs show no attraction (including observations in \citep{bertin_h3_2007}). 
This suggests the major role of H4 tail in mediating inter-NCP attractions.

\subsubsection{Discussion}
We consider the quantification of inter-NCP pair potentials from SAXS structure factors as the most interesting result of our study.
Compared with the traditional analysis of second virial coefficients that relies on $I(Q = 0)$ only, the GOCM (and methods alike) uses the full SAXS profiles and obtains self-consistent pair potentials allowing quantitative comparisons with theoretical predictions. 
At low salts such as \SI{10}{mM} KCl, NCP-NCP interactions are dominated by repulsion as expected. 
Essentially the same $Z_\text{eff}$ is obtained for all four NCP constructs. 
The minimal effect of tail deletions on inter-NCP repulsion is not surprising given that tail deletions change NCP bare charges by $<8\%$ and that cations can effectively fill in for the deleted monocharged amino acids. 
Quantitatively, it is instructive to compare the measured $Z_\text{eff}$ with the theoretical renormalized charge $Z_\text{ren}$ that can be calculated following the charge renormalization theorem \citep{trizac_alexanders_2003}. 
For dsDNA with negatively charged groups only, our previous work showed that the measured $Z_\text{eff}$ is fairly consistent (within 10--25\%) with its renormalized charge $Z_\text{ren}$ in monovalent salts \citep{qiu_measuring_2006}. 
However, for the nucleosome, its $Z_\text{eff}$ (\textapprox\SI{22}{\textit{e}}) is far smaller than theoretical $Z_\text{ren}$ ($>\SI{100}{\textit{e}}$ for all experimental conditions), noting that $Z_\text{bare}$ is \textapprox\SI{150}{\textit{e}} and $Z_\text{ren}$ weakly depends on NCP and salt concentrations \citep{trizac_alexanders_2003}. 
Given that the $Z_\text{ren}$ is calculated by treating NCP as a uniformly charged sphere, such drastic and physically significant discrepancy is likely due to the charge nonuniformity of NCP in terms of spatial distribution and sign, resulting in highly anisotropic behaviors where the uniformly charged surface consideration breaks down. 

Inter-NCP interactions at short range are further complicated by the flexible histone tails \citep{korolev_modelling_2012, arya_tale_2009}.
At elevated salts where electrostatic repulsion is greatly weakened, histone tail deletions can significantly affect NCP-NCP interactions.
At \SI{100}{mM} KCl (\cref{fig:mono:2} {\bf c}, \cref{fig:mono:3} {\bf b} and {\bf d}), NS-NCP shows significant attraction (i.e., low $Q$ upturn), gH3 RC-NCP shows attraction of comparable magnitude, and gH4 RC-NCP shows nearly no attraction. 
The difference between gH3 and gH4 RC-NCPs, which have not been studied by SAXS previously, suggests that the H4 tail is more important in mediating NCP-NCP attractions than the H3 tail. 
This corresponds well with the observed most important role of the H4 tail in assembling the 30-nm chromatin fiber \citep{dorigo_chromatin_2003}. 
Such trend is also consistent with the repulsion between gH3gH4 RC-NCPs up to \SI{200}{mM} KCl \citep{bertin_h3_2007}. 
The more important role of the H4 tail in mediating internucleosome interactions has been suggested by Arya and Schlick \citep{arya_role_2006} analyzing a CG mesoscale structure model with a tailored configurational-bias Monte Carlo method. 
As H4 tail deletion removes fewer charges than H3 tail deletion ($+3$ versus $+\SI{10}{\textit{e}}$) but shows a larger effect, this alludes to specific interactions of H4 tails with adjacent NCPs in close approach. 
To probe the likely H4-tail-DNA interactions, Korolev and Nordenski\"old \citep{korolev_h4_2007} performed all-atomic molecular dynamics simulations of DNA and H4 tails and suggested possible lysine-mediated bridging and minor groove dynamic binding. 
Another possible explanation is H4-tail binding with the H2A/H2B anionic patch on neighboring NCPs, as suggested by crystallography studies \citep{dorigo_nucleosome_2004,luger_crystal_1997}.
This is in agreement with recent experimental studies of the effects of H4 tail lysine acetylation on nucleosome array folding that proposed site-specific lysine binding to the H2B histone \citep{allahverdi_effects_2011}. 
To fully establish the underlying molecular mechanisms, experimental approaches probing the local structure would be needed to resolve the physical origins of inter-NCP short range attraction.

\subsubsection{Conclusion}
In conclusion, we have measured repulsive and attractive NCP-NCP interactions under various salt conditions with distinct NCP constructs. 
Quantitative knowledge of inter-NCP pair potentials should aid theoretical studies of nucleosome and chromatin high-order structure and dynamics. 
The physical understanding to be gained will shed light on the broad class of multiphasic macromolecules with two or more different interacting groups. 
Work is underway to relate the inter-NCP interactions to the conformation and energetics of nucleosome arrays through measurement and modeling.

\subsection{Chapter Summary}
This initial study of single nucleosome structure and interaction establishes the provides an understanding of the internucleosome interactions underpinning the more complicated study of nucleosome arrays.
The agreement between the NS-NCP form factor and the calculated 1KX5 crystal NCP \citep{davey_solvent_2002} form factor indicates that it is reasonable to expect the nucleosome structure within the array to be relatively unchanged under the same ionic conditions.
This process of comparing the experimental and calculated form factors introduces the concept of matching atomistic structures to SAXS results, a fundamental process for \hyperref[chp:array]{analyzing the nucleosome arrays}.
Recognizing that the electrostatic interactions of nucleosomes are a small fraction of the theoretically renormalized charge provides evidence that nucleosomes within an array may not repel each other as much as would be expected in the case of a uniform charge distribution.
Lastly, the experience gained in these preliminary studies was crucial to the becoming familiar with the measurement and analysis process to be discussed further in the \hyperref[chp:saxs]{following chapter}.



