% $Id$
\subsection{C: CG MC Simulation Algorithm for ds-DNA Supplementary Material}\label{app:sassie}

Figures \ref{fig:alpha}--\ref{fig:chi} show the dihedral angle results from the \SI{75}{ns} MD simulation of a solvated and ionized 12 bp B-form DNA model (PDB ID: 119D \citep{leonard_crystal_1993}).
We calculated the seven dihedral angles for the ten central bp of the model (excluding the first and last bp), every \SI{0.01}{ns}.
To prevent wraparound, the y-axis in each plot is offset so the median angle is zero and each dot represents the deviation from that median angle.
The dark and light gray dots respectively represent DNA bases with BI and BII conformations (BI: $\varepsilon-\zeta > 0$ and BII: $\varepsilon-\zeta > 0$).
The horizontal dashed lines indicate 2 standard deviations from the mean dihedral angle for all the bases (red), the BI bases (teal), and the BII bases (yellow).
For the $\chi$ angle we separate the BI bases into purines (teal) and pyrimidines (royal blue) for direct comparison with the survey of crystal structures performed by Schneider et al.~\citep{schneider_conformations_1997}.
The solid lines show the corresponding rolling average calculated at each MD time step. 
The mean values and standard deviations are reported in the legends. 

Table \ref{tab:MD_simulation} summarizes these results in comparison with those from the survey of 34 B-form DNA crystal structures \citep{schneider_conformations_1997}.
The mean angles obtained from this MD simulation match reasonably well to those from the crystal structures; the distribution of angles in broader for the MD results.

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\textwidth]{sassie_figures/c36_MD/med/alpha.png}
  \caption{\label{fig:alpha}
    DNA Dihedral Angle $\alpha$ from the \SI{75}{ns} MD simulation
  }
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\textwidth]{sassie_figures/c36_MD/med/beta.png}
  \caption{\label{fig:beta}
    DNA Dihedral Angle $\beta$ from the \SI{75}{ns} MD simulation
  }
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\textwidth]{sassie_figures/c36_MD/med/gamma.png}
  \caption{\label{fig:gamma}
    DNA Dihedral Angle $\gamma$ from the \SI{75}{ns} MD simulation
  }
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\textwidth]{sassie_figures/c36_MD/med/delta.png}
  \caption{\label{fig:delta}
    DNA Dihedral Angle $\delta$ from the \SI{75}{ns} MD simulation
  }
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\textwidth]{sassie_figures/c36_MD/med/epsilon.png}
  \caption{\label{fig:epsilon}
    DNA Dihedral Angle $\varepsilon$ from the \SI{75}{ns} MD simulation
  }
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\textwidth]{sassie_figures/c36_MD/med/zeta.png}
  \caption{\label{fig:zeta}
    DNA Dihedral Angle $\zeta$ from the \SI{75}{ns} MD simulation
  }
\end{figure}

\begin{figure}[!htb]
  \centering
  \includegraphics[width=\textwidth]{sassie_figures/c36_MD/med/chi.png}
  \caption{\label{fig:chi}
    DNA Dihedral Angle $\chi$ from the \SI{75}{ns} MD simulation
  }
\end{figure}

\begin{figure}[htb!]
  \centering
  \begin{overpic}[width=0.8\textwidth]{sassie_figures/m_x100_in_range_MD}
    \put(30,30){\includegraphics[width=0.5\textwidth]{sassie_figures/min_affect.png}}
  \end{overpic}
  \caption[{Iterative Minimization of dsDNA}]{\label{fig:min_affect}
    \textbf{Iterative Minimization of dsDNA:}
    Percent of DNA dihedral angles that are within 2 standard deviations of the mean dihedral angles calculated in the \SI{75}{ns} MD simulation of dsDNA.
    This percentage is plotted as a function of the number of iterations of a 2000 step minimization procedure using CHARMM36 \citep{hart_optimization_2012} force field.
    The dashed line shows the 95\% cutoff for the percent of dihedral angles that should be within 2 standard deviations of the mean for a normal distribution.    
    The inset DNA illustrates the DNA structure before minimization (white) and after 10 minimization iterations or 20000 total minimization steps (red).
    The dihedral angles for these structures do not exceed the normal range until around 70 repeated cycles, 140000 minimization steps, but the illustration shows that after as few as 10 minimization cycles, when almost 100 percent of each of dihedral angles are within their acceptable ranges, the DNA has experienced significant deformation of the major and minor groove.
    Additional minimization cycles only compounds this deformation of the DNA structure.
    Consequently, we recommend performing a single iteration of 2000 minimization steps after completing all MC simulations.
    Figure \ref{fig:in_range_dihedrals} shows that this is sufficient to relax the strain between the O3' atom of one base and the P atom of the following base that results from the CG process.
    When using a structure from a previous MC simulation as the starting structure for a new MC simulation, one should use the non-minimized structure to prevent this deformation of the DNA major and minor groove.
  }
\end{figure}

\newgeometry{top=1in, bottom=1in, left=1in, right=1in}
\renewcommand{\thesubfigure}{\alph{subfigure}}
\begin{figure}[!hp]
  \begin{subfigure}[b]{\textwidth}
    \begin{subfigure}[b]{0.325\textwidth}
      \adjincludegraphics[width=\textwidth,trim={0 {0.22\height} 0 {0.24\height}},clip]{sassie_figures/den_all_12}
      \caption{NCP$_1$ All}
    \end{subfigure}
    \begin{subfigure}[b]{0.325\textwidth}
      \adjincludegraphics[width=\textwidth,trim={0 {0.22\height} 0 {0.24\height}},clip]{sassie_figures/den_best_12}
      \caption{NCP$_1$ Best}
    \end{subfigure}
    \begin{subfigure}[b]{0.325\textwidth}
      \adjincludegraphics[width=\textwidth,trim={0 {0.22\height} 0 {0.24\height}},clip]{sassie_figures/den_all_vs_best_12}
      \caption{NCP$_1$ All vs Best}
    \end{subfigure}
  \end{subfigure}
  \begin{subfigure}[b]{\textwidth}
    \begin{subfigure}[b]{0.325\textwidth}
      \adjincludegraphics[width=\textwidth,trim={0 {0.22\height} 0 {0.25\height}},clip]{sassie_figures/den_all_23}
      \caption{NCP$_3$ All}
    \end{subfigure}
    \begin{subfigure}[b]{0.325\textwidth}
      \adjincludegraphics[width=\textwidth,trim={0 {0.22\height} 0 {0.25\height}},clip]{sassie_figures/den_best_23}
      \caption{NCP$_3$ Best}
    \end{subfigure}
    \begin{subfigure}[b]{0.325\textwidth}
      \adjincludegraphics[width=\textwidth,trim={0 {0.22\height} 0 {0.25\height}},clip]{sassie_figures/den_all_vs_best_23}
      \caption{NCP$_3$ All vs Best}
    \end{subfigure}
  \end{subfigure}
  \begin{subfigure}[b]{\textwidth}
    \begin{subfigure}[b]{0.325\textwidth}
      \adjincludegraphics[width=\textwidth,trim={0 {0.22\height} 0 {0.24\height}},clip]{sassie_figures/den_all_123}
      \caption{NCP$_1$ \& NCP$_3$ All}
    \end{subfigure}
    \begin{subfigure}[b]{0.325\textwidth}
      \adjincludegraphics[width=\textwidth,trim={0 {0.22\height} 0 {0.24\height}},clip]{sassie_figures/den_best_123}
      \caption{NCP$_1$ \& NCP$_3$ Best}
    \end{subfigure}
    \begin{subfigure}[b]{0.325\textwidth}
      \adjincludegraphics[width=\textwidth,trim={0 {0.22\height} 0 {0.24\height}},clip]{sassie_figures/den_all_vs_best_123}
      \caption{NCP$_1$ \& NCP$_3$ All vs Best}
    \end{subfigure}
  \end{subfigure}
  \begin{subfigure}[b]{\textwidth}
    \begin{subfigure}[b]{0.325\textwidth}
      \includegraphics[width=\textwidth]{sassie_figures/den_all_24}
      \caption{NCP$_4$ All}
    \end{subfigure}
    \begin{subfigure}[b]{0.325\textwidth}
      \includegraphics[width=\textwidth]{sassie_figures/den_best_24}
      \caption{NCP$_4$ Best}
    \end{subfigure}
    \begin{subfigure}[b]{0.325\textwidth}
      \includegraphics[width=\textwidth]{sassie_figures/den_all_vs_best_24}
      \caption{NCP$_4$ All vs Best}
    \end{subfigure}
  \end{subfigure}
  \begin{subfigure}[b]{\textwidth}
    \begin{subfigure}[b]{0.325\textwidth}
      \includegraphics[width=\textwidth]{sassie_figures/den_all_1234}
      \caption{All NCPs, All Structures}
    \end{subfigure}
    \begin{subfigure}[b]{0.325\textwidth}
      \includegraphics[width=\textwidth]{sassie_figures/den_best_1234}
      \caption{All NCPs, Best Structures}
    \end{subfigure}
    \begin{subfigure}[b]{0.325\textwidth}
    \end{subfigure}
  \end{subfigure}
  \caption[Spatial Range of the Complete vs Best Ensembles]{\label{fig:sup:4x167example}
    \textbf{Spatial Range of the Complete vs Best Ensembles:}
    Visualizations of the spatial range envelopes of each NCP in the nucleosome array, overlain on an illustration of an example structure from the best 500 structures.
    As a reference point, each structure in the ensemble was aligned using the second NCP in the array (NCP$_2$), illustrated with a blue protein core.
    In these illustrations, we position NCP$_2$ closest to the bottom of the page with the NCP dyad oriented toward the top of the page.
    Each spatial range is shown using two views, the one on the left looking at the face of NCP$_2$ and the one on the right looking at the side of NCP$_2$.
    As there are two flexible linkers between NCP$_4$ and the aligned NCP, NCP$_2$, the spatial range covered by NCP$_4$ obscures the view of NCP$_1$ and NCP$_3$.
    We therefore separate the spatial ranges of NCP$_1$ (red), NCP$_3$ (yellow), and NCP$_4$ (purple) as follows.
    The first row shows only the range of NCP$_1$, (a)--(c).
    The second row shows only the range of NCP$_3$, (d)--(f).
    The third row show the ranges of both NCP$_1$ and NCP$_3$, (g)--(i).
    The fourth row shows only the range of NCP$_4$, (j)--(l).
    The fifth row show the combined ranges of all NCPs, (m) and (n).
    For all rows, the first column shows the range of the complete ensemble---(a), (d), (g), (j), and (m)---the second column shows the range of the sub-ensemble of the best 500 structures---(b), (e), (h), (k), and (n)---and the third column compares the range of the complete ensemble with the best sub-ensemble---(c), (f), (i), and (l).
    We exclude what would be panel (o), an overlain image of the ranges with all NCPs, because of an inability to show sufficient levels of transparency).
  }
\end{figure}

\newgeometry{top=1in, bottom=1in, left=1.25in, right=1.25in}

\begin{figure}[!htbp]
  \includegraphics[width=\textwidth]{sassie_figures/convergence_by_ncp}
  \caption[ConveNCP NCP in the Tetranucleosome Array Ensemble]{\label{fig:sup:convergence}
    \textbf{Convergence of Each NCP in the Tetranucleosome Array Ensemble:}
    Separating the spatial convergence of the tetranucleosome ensemble, shown in \cref{fig:tetranucleosome}(c), by each of the NCPs in thewe verify good convergence for all four NCPs.
    This is demonstrated by the consistent number of occupied voxels as additional structures are added to the ensemble.
    }
\end{figure}
