\documentclass{beamer}

\usetheme{Frankfurt} % AnnArbor Antibes Bergen Berkeley Berlin Boadilla boxes CambridgeUS Copenhagen Darmstadt default Dresden Frankfurt Goettingen Hannover Ilmenau JuanLesPins Luebeck Madrid Malmoe Marburg Montpellier PaloAlto Pittsburgh Rochester Singapore Szeged Warsaw
\usecolortheme{beetle} % albatross beaver beetle crane default dolphin dove fly lily orchid rose seagull seahorse sidebartab structure whale wolverine
%\usefonttheme{structuresmallcapsserif} % default professionalfonts serif structurebold structureitalicserif structuresmallcapsserif
%\usefonttheme[onlymath]{serif}
\usefonttheme{serif}

%%%%% Special Stuff %%%%%
\usepackage{textcomp}
\newcommand{\textapprox}{\raisebox{0.5ex}{\texttildelow}} % together with the textcomp package, this provides a good ~ symbol
\usepackage{url}
\usepackage[controls,autoplay,loop]{animate}
% \usepackage{cleveref}
% \usepackage{amsmath}
% \usepackage{amssymb}
\usepackage{media9}
\PassOptionsToPackage{noplaybutton}{media9}
\usepackage{bm}
\newcommand*\mean[1]{\bar{#1}}
\usepackage{graphicx}
\usepackage[export]{adjustbox} %s enables image cropping
% \usepackage{tikz} % for adding solid background color to images with transparent background
\renewcommand{\arraystretch}{1.3}
\usepackage{caption}
\usepackage{subcaption} %s enables creating subfigures

% color underline
\usepackage{soul}
\newcommand{\myul}[2][black]{\setulcolor{#1}\ul{#2}\setulcolor{black}}
\definecolor{myblue}{RGB}{56, 97, 138}
\definecolor{myred}{RGB}{217, 61, 62}
\definecolor{mygreen}{RGB}{75, 108, 35}
\definecolor{mypurple}{RGB}{100, 68, 117}
\definecolor{myyellow}{RGB}{228, 182, 48}
\definecolor{mybrown}{RGB}{183, 92, 56}
\definecolor{mylblue}{RGB}{107, 171, 215}
\definecolor{mypink}{RGB}{209, 84, 175}
\definecolor{mylgreen}{RGB}{177, 191, 57}
\definecolor{mylpurple}{RGB}{126, 116, 209}

\usepackage{multirow} % multirow table element
\usepackage{multicol} % for multicolumn itemize
\usepackage{booktabs} % for line added control of lines in tables
%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Dynamic Conformations of Nucleosome Arrays in Solution from Small-Angle X-ray Scattering}
\author{by Steven C. Howell}
% \subtitle{Using Beamer}
\institute{The George Washington University}
\date{November 20, 2015}

\begin{document}

\begin{frame}{}
  \titlepage
\end{frame}

\begin{frame}{Outline}
  \tableofcontents[hideallsubsections]
\end{frame}

% \begin{frame}{Outline}
%   %\begin{block}{Outline}
%     \begin{enumerate}
%     \item Chromatin: DNA in Eukaryotic Cells
%     \item Small-Angle X-ray Scattering (SAXS)
%     \item Experimental SAXS Results
%     \item Monte Carlo (MC) Modeling of Coarse Grain (CG) DNA
%     \item Modeling 4$\times$167 gH5 Arrays
%     \end{enumerate}
%   %\end{block}
% \end{frame}

% ~~~~~~~~~~~~~~~~~~~~~~~~~~ Chromatin Background ~~~~~~~~~~~~~~~~~~~~~~~~~
\section{Chromatin \& Nucleosome Arrays}

\subsection{Chromatin: Highly Packaged DNA}
\begin{frame}{Chromatin is highly packaged DNA.}
  \begin{columns}

    \column{0.45\textwidth}
    \begin{columns}
      \column{0.9\textwidth}
      \includegraphics[width=\textwidth]{figures/background/chromatin_vs_chromosome}\\
      \centering
      \vspace{-1.7mm}
      \begin{columns}
        \column{0.9\textwidth}
        \TINY{\url{http://micro.magnet.fsu.edu/cells/nucleus/images/chromatinstructurefigure1.jpg}}
      \end{columns}
    \end{columns}
    \onslide<3->{
      \includegraphics[width=\textwidth]{figures/background/chromatin_inv}
      % \includegraphics[width=\textwidth]{figures/background/chromatin_orig}\\
    }

    \column{0.5\textwidth}
    \begin{block}{DNA in Your Cells:}
      \begin{itemize}
      \item $6\times10^9$ base pairs
      \item \textapprox2 m stacked end-to-end
      \end{itemize}
    \end{block}

    \onslide<2->{
      \begin{block}{DNA in Your Body:}
        \begin{itemize}
        \item \textapprox$100 \times 10^9$ km
        \item \textapprox$670 \times \text{distance to the sun}$
        \end{itemize}
      \end{block}
    }
    \onslide<4>{
      \begin{block}{Packaging purposes:}
        \begin{itemize}
        \item Organized Compaction
        \item Protection
        \item Regulation
        \end{itemize}
      \end{block}
    }  
  \end{columns}

\end{frame}

\begin{frame}{Chromatin structure impacts genetic processes.}
    \begin{columns}
      \column{0.65\textwidth}
      \includegraphics[width=\textwidth]{figures/background/genetically_identical_mice}\\
      \tiny{Duhl et al.~\textit{Nature Genetics}, 1994; DOI: \href{http://www.nature.com/ng/journal/v8/n1/abs/ng0994-59.html}{10.1038/ng0994-59}}.
      \column{0.35\textwidth}
      \begin{block}{Structure affects genetic:}
        \begin{itemize}
        \item Expression
        \item Replication
        \item Repair
        \end{itemize}
      \end{block}
      
    \end{columns}
\end{frame}

\begin{frame}{Packaging of nucleosomes is not well understood.}
  \begin{columns}
    \column{0.9\paperwidth}
    \begin{columns}
      \column{0.5\textwidth}
      \setbeamercovered{transparent}
      \begin{itemize}[<+->]
      \item DNA Structure Solved {\scriptsize(Watson, Crick, Wilkins, and Franklin) }
      \item Nucleosome Crystal Structure Solved {\scriptsize(Luger, Richmond, et al.)}
      \item Some Success in Crystallizing Nucleosome Arrays {\scriptsize(Schalch, Richmond, et al.)}
      \end{itemize}
      \column{0.1\textwidth}
      \only<1->{
        \centering
        %\includegraphics[height=0.7\paperheight]{figures/background/dsDNA_white_tran}
        \includegraphics[width=\textwidth]{figures/background/dsDNA_black_tran}
        %\includegraphics[height=0.7\paperheight]{figures/background/dsDNA_beetle}
      }
      \column{0.4\textwidth}
      \only<2->{
        \centering
        \includegraphics[width=0.67\textwidth]{figures/background/1kx5Diagram_noTails}\\
        \tiny{Representation of PDB ID: \href{http://www.rcsb.org/pdb/explore.do?structureId=1kx5}{1KX5}}      
      }
      \onslide<3>{
        \centering
        \includegraphics[width=\textwidth]{figures/background/newTetraDiagram}\\
        \tiny{Representation of PDB ID: \href{http://www.rcsb.org/pdb/explore.do?structureId=1zbb}{1ZBB}}      
      }
    \end{columns}
  \end{columns}
\end{frame}

\begin{frame}{The cellular environment is dynamic.}
  \begin{center}
    {\color{white} 
      How does the structure of nucleosomes in solution \\
      differ from the crystal structures?\\}
  \end{center}
  \hspace{1cm}
  \begin{columns}
    \column{.4\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/background/1kx5Diagram_noTails}\\
    \tiny{Representation of PDB ID: \href{http://www.rcsb.org/pdb/explore.do?structureId=1kx5}{1KX5}}      
    \column{.6\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/background/newTetraDiagram}\\
    \tiny{Representation of PDB ID: \href{http://www.rcsb.org/pdb/explore.do?structureId=1zbb}{1ZBB}}      
  \end{columns}
\end{frame}

% \subsection{``Beads-on-a-String''}
\begin{frame}{Nucleosome packing is like folding ``beads-on-a-string.''}
  \begin{columns}
    \column{.5\textwidth}
    \only<2-3>{
      \begin{block}{}
        \setbeamercovered{transparent}
        \begin{itemize}
        \item {\color{white} Bead:} Nucleosome Core Particle (NCP)
          \onslide<3>{\item {\color{white} String:} 10--90 bp linker DNA}
        \end{itemize}
      \end{block}{}
    }
    \only<4->{
      \begin{block}{Modulating Factors Considered:}
        \begin{itemize}
        \item Ionic Screening
          {\scriptsize
            \begin{itemize}
            \item  10 mM K$^+$ 
            \item 1 mM Mg$^{2+}$
            \end{itemize}
          }
        \item Linker Histone (gH5)
        % \item Linker Length
        \end{itemize}
      
      \end{block}
    }
    \column{.25\textwidth}
    \onslide<2->{
      \centering
      \includegraphics[width=1.05\textwidth]{figures/background/1kx5Diagram_noTails}\\
      \tiny{Representation of\\PDB ID: \href{http://www.rcsb.org/pdb/explore.do?structureId=1kx5}{1KX5}}
    }
    \column{.15\textwidth}
    \onslide<3->{
      \adjincludegraphics[height=3cm,trim={{0.18\width} 0 0 0},clip]{figures/background/linker_dna}
    }
  \end{columns}
  \vspace{0.3cm}
  \hspace*{-1.5cm}
  \includegraphics[width=1.1\paperwidth]{figures/background/beads-on-a-string}


\end{frame}

% ~~~~~~~~~~~~~~~~~~~~~~~~~~ SAXS Results ~~~~~~~~~~~~~~~~~~~~~~~~~
\section{SAXS Results}
\subsection{SAXS Method}
\begin{frame}{\normalsize Small-Angle X-ray Scattering (SAXS) measures interparticle distances.}
  % \begin{tikzpicture}
  %   \node[fill=white]{\includegraphics[width=\textwidth]{figures/background/scattering_diagram}};
  % \end{tikzpicture}
  \hspace*{-3cm}
  \includegraphics[width=1.13\paperwidth]{figures/background/scattering_diagram_tran}\\
  \begin{align*}
    \text{\color{white} Momentum Transfer:} \quad& Q\,\,\quad = 4\pi\dfrac{sin(\vartheta)}{\lambda}\\
    \text{\color{white} Scattering Intensity:} \quad& I(Q) = 
    \only<1>{P(Q)\,S(Q)}
    \onslide<2->{4\pi\int_0^\infty \!\!\!P(r) \frac{\sin(Q\,r)}{Q\,r}\, dr\\
      \text{\color{white} Pair-Distance Distribution:} \quad& P(r) = \dfrac{1}{2\pi^2}\int_0^\infty\!\!\!  I(Q)\,\sin(Q\,r) \,Q\,r\, dQ}
  \end{align*}
\end{frame}

\subsection{Interpreting experimental results requires modeling.}
%\begin{frame}[t]{\normalsize Addition of gH5 changes the 4$\times$167 structure under high ionic screening.}
\begin{frame}[t]{\normalsize The 1ZBB model best describes the 4$\times$167 gH5 array in 1 mM Mg$^{2+}$.}
  % $\text{\color{white} Bragg Equation: }D_\text{repeat} = \left.2\pi\right/Q$
  \vspace{0.3cm}
  \begin{columns}
    \column{\paperwidth}
    \hspace*{0.3cm}\adjincludegraphics[height=3.6cm,trim={0 0 {0.67\width} 0},clip]{figures/array_data/4x167_gH5_iq_kratky_pr}
    %\hspace(0.2cm}
    \onslide<2->{
      \hspace*{0.1cm}\adjincludegraphics[height=3.6cm,trim={{0.33\width} 0 {0.33\width} 0},clip]{figures/array_data/4x167_gH5_iq_kratky_pr}}
    %\hspace(0.2cm}
    \onslide<3->{
      \hspace*{0.1cm}\adjincludegraphics[height=3.6cm,trim={{0.67\width} 0 0 0},clip]{figures/array_data/4x167_gH5_iq_kratky_pr}}
  \end{columns}
  \begin{columns}
    \column{0.95\paperwidth}
    \hspace*{0.2cm}\begin{block}{}
      \only<1-3>{
        \begin{itemize}
        % \item {\color{white} NCP Region:} 0.1 \AA$^{-1}$ $<Q<$ 0.2 \AA$^{-1}\quad \leftrightarrow\quad 60 \text{ \AA} > D > 30 \text{ \AA}$
          %\only<2-3>{
          % \pause
        \item {\color{white} Effect of ionic screening:}
          \begin{itemize}
          \item Peak Shift: 0.040 \AA$^{-1}$ in K$^+\rightarrow$ 0.018 \AA$^{-1}$ in Mg$^{2+}$
          %\only<3>{
          % \pause
          \item Domains Merge: multi-domain in K$^+\rightarrow$ single-domain in Mg$^{2+}$
          \end{itemize}
        \item {\color{white} Effect of gH5:} 
          \begin{itemize}
          \item gH5 changes the 4$\times$167 structure under high ionic screening.          
          \end{itemize}
        \item \textbf{\color{myyellow} 1ZBB:} matches the \textbf{\color{mygreen} gH5 in 1 mM Mg$^{2+}$} closest.
        \end{itemize}
      }
    \end{block}
  \end{columns}

\end{frame}




% ~~~~~~~~~~~~~~~~~~~~~~~~~~ MC Modeling of CG DNA ~~~~~~~~~~~~~~~~~~~~~~~~~
\section{MC Modeling of CG DNA}

\subsection{Bending Energetics}
\begin{frame}[t]{\normalsize MC algorithm performs Metropolis sampling of a Markov Chain.}
  \vspace*{-3mm}
  \begin{block}{Wormlike Bead--Rod Model\\{\scriptsize (Dorfman group, U.~of Minnesota)}}
    \vspace*{-5mm}
    \setbeamercovered{transparent}
    \begin{align*}
      \action<+->{U_\text{bend} &= k_BT \left(\left.L_p\right/l\right)\sum\left(1-\bm{u}_k\cdot\bm{u}_{k+1}\right)\\}
      \action<+->{U_\text{WCA}} & \action<.->{= 
        \begin{cases}
          4k_BT \left[\left(\sigma/r_{ij}\right)^{12} - \left(\sigma/r_{ij}\right)^6 + 1/4 \right]} & \action<.->{r_{ij}<2^{1/6}\sigma\\
          0} & \action<.->{r_{ij} \geq 2^{1/6}\sigma
      \end{cases}}
    \end{align*}
  \end{block}
  \only<1-2>{
    \centering
    \includegraphics[width=\textwidth]{figures/bead-rod_model_bend}
  }
  \only<3>{
    \begin{columns}
      \column{.4\textwidth}
      \begin{block}{}
        \begin{itemize}
        \item $l = \begin{cases} 51.6\text{ \AA}\\3.38\text{ \AA}\end{cases}$
        \item $\sigma=2^{-1/6}\,l\approx 0.89\,l$
        \end{itemize}
      \end{block}
      \column{.5\textwidth}
      \includegraphics[width=5cm]{sassie_figures/validate_model_lin_mod}
    \end{columns}
  }
\end{frame}

\subsection{Twist Energetics}
\begin{frame}[t]{\normalsize MC algorithm performs Metropolis sampling of a Markov Chain.}
  \vspace*{-3mm}
  \begin{block}{Harmonic Twist Model\\{\scriptsize (Olson, Rutgers U.~\& Zhurkin, NIH)}}
  \vspace*{-5mm}
    \begin{align*}
      U_\text{twist} &= \dfrac{k_BT}{2} \kappa \sum_{k=1}^{N-1}(\omega_{k}-\mean{\omega})^2
    \end{align*}
  \end{block}
  \only<1>{
    \centering
    \includegraphics[width=\textwidth]{figures/bead-rod_model_twist}
  }
  \only<2>{
    \begin{columns}
      \column{.4\textwidth}
      \begin{block}{}
        \begin{itemize}
        \item $\kappa = 0.062 \times (1^\circ)^{-2}$
        \item $\mean{\omega}=35.4^\circ$
        \end{itemize}
      \end{block}
      \column{.5\textwidth}
      \includegraphics[width=5.5cm]{sassie_figures/c36_dna_350k_sparse}
    \end{columns}
  }
\end{frame}

\subsection{Algorithm Process}
\begin{frame}{\hspace{-1mm}\small CG MC with energy minimization produces physically realizable atomic models.}
  \hspace*{-1.09cm}
  \includegraphics[width=\paperwidth]{figures/flow_chart_landscape}
\end{frame}

\subsection{Minimization}
\begin{frame}{CG MC moves strain the O3$'$--P bond.} %Resolving CG Strains}
  \begin{columns}
    \column{.5\textwidth}
    \vspace{7mm}
    \begin{block}{Raw MC:}
      \includegraphics[width=\textwidth]{figures/min_example_raw}
    \end{block}
    \column{.5\textwidth}
    \only<1>{\hspace*{-4.5mm}\includegraphics[width=1.2\textwidth]{sassie_figures/DNA_dihedral_labels}}
    \only<2>{
      \vspace{7mm}
      \begin{block}{Minimized MC:}
        \includegraphics[width=\textwidth]{figures/min_example_min}
      \end{block}
    }
  \end{columns}
\end{frame}

% ~~~~~~~~~~~~~~~~~~~~~~~~~~ Modeling Results ~~~~~~~~~~~~~~~~~~~~~~~~~
\section{Modeling Results}

\subsection{Structure Ensemble}
\begin{frame}{\large The structure ensemble samples a wide variety of configurations.}
  \begin{columns}
    \column{0.45\paperwidth}
    \only<1>{
      \includemedia[
        width=\linewidth,
        height=.75\linewidth,
        activate=pageopen,
        addresource=videos/4x167_all.mp4,
        flashvars={
          source=videos/4x167_all.mp4
          &loop=true                       % loop video
        }
      ]{}{VPlayer.swf}
    }

    \only<2->{
      \begin{figure}[htb]
        \begin{subfigure}[b]{\linewidth}
          \adjincludegraphics[width=\textwidth,trim={0 {0.33\height} 0 {0.28\height}},clip]{figures/array_models/density_all_12}
          \vspace{-12mm}
          \subcaption*{NCP$_1$}
          \vspace{1mm}
        \end{subfigure}
        \begin{subfigure}[b]{\linewidth}
          \adjincludegraphics[width=\textwidth,trim={0 {0.33\height} 0 {0.28\height}},clip]{figures/array_models/density_all_23}
          \vspace{-12mm}
          \subcaption*{NCP$_3$}
          \vspace{1mm}
        \end{subfigure}
        \begin{subfigure}[b]{\linewidth}
          \adjincludegraphics[width=\textwidth,trim={0 {0.2\height} 0 {0.13\height}},clip]{figures/array_models/density_all_24}
          \vspace{-12mm}
          \subcaption*{NCP$_4$}
        \end{subfigure}
      \end{figure}
    }

    \column{0.45\paperwidth}
    \begin{figure}[htb]
      \includegraphics[width=\textwidth]{figures/array_models/density_all_1234}
    \end{figure}
  \end{columns}
\end{frame}

\subsection{4$\times$167 gH5 in 10 mM K$^{+}$}
\begin{frame}[t]{\normalsize Individual structures reproduce the 10 mM K$^{+}$ scattering.}
  \begin{center}
    % \vspace{-5mm}
    \includegraphics[height=0.4\paperheight]{figures/array_models/i1_r_c000_4x167_h5_k010_33_fit}\\
    \only<1>{
      \vspace{2mm}
      \begin{align*}
        R = \dfrac{\displaystyle\sum_{i=1}^{N_Q}\Bigl|I_e(Q_i)-c_\text{scale}\,I_m(Q_i)\Bigr|}{\displaystyle\sum_{i=1}^{N_Q}\Bigl|I_e(Q_i)\Bigr|}\,,\qquad  c_\text{scale}=\dfrac{I_e(0)}{I_m(0)}
      \end{align*}
      }
    \only<2>{
      \includegraphics[height=0.4\paperheight]{figures/array_models/i1_r_best1000_c000_4x167_h5_k010_33_fit}
    }
  \end{center}
\end{frame}

\subsection{4$\times$167 gH5 in 1 mM Mg$^{2+}$}
\begin{frame}[t]{\normalsize Individual structures reproduce the 1 mM Mg$^{2+}$ scattering.}
  \begin{center}
    \includegraphics[height=0.4\paperheight]{figures/array_models/i1_r_c000_4x167_h5_mg1_33_fit}\\
    \onslide<2>{
      \includegraphics[height=0.4\paperheight]{figures/array_models/i1_r_best1000_c000_4x167_h5_mg1_33_fit}
    }
  \end{center}
\end{frame}

\begin{frame}[t]{\normalsize 1 mM Mg$^{2+}$ ensemble resembles loosely packed 1ZBB models.}
  \begin{columns}
    %\column{\dimexpr\paperwidth-12pt}
    \column{0.5\paperwidth}
    \onslide<2>{
      \renewcommand{\arraystretch}{1.2}
      \scriptsize
      \begin{tabular}{  r l  r l  r l }
        \multicolumn{2}{c}{}                                                                     & \multicolumn{2}{c}{\shortstack[1]{\tiny Stack\\\tiny Distance}}        & \multicolumn{2}{c}{\shortstack[1]{\tiny Angle Between\\\tiny Cylinder Axes}} \\[-1.2mm] \cmidrule{2-6}
        % \multicolumn{2}{c}{}                                                                     & \multicolumn{2}{c}{\shortstack[1]{ Stack\\ Distance}}        & \multicolumn{2}{c}{\shortstack[1]{ Angle Between\\ Cylinder Axes}} \\[-1.2mm] \cmidrule{2-6}
        % \multirow{3}{*}{\rotatebox{90}{\tiny 10 mM K$^+$}}    & \!\!NCP$_1$--NCP$_3$     \!\!\!\!&\!\!\!\!  95.2 \!\!\!\!& $\!\!\!\!\!\!\pm 2.5$  \AA\, \!\!\!\!\!\!\!\!& $10.9^\circ$ \!\!\!\!\!& $\!\!\!\!\!\!\pm 3.9^\circ$  \\
        % ~                                                     & \!\!NCP$_2$--NCP$_4$     \!\!\!\!&\!\!\!\! 247.3 \!\!\!\!& $\!\!\!\!\!\!\pm 12.9$ \AA\, \!\!\!\!\!\!\!\!& $38.5^\circ$ \!\!\!\!\!& $\!\!\!\!\!\!\pm 23.5^\circ$ \\
        % ~                                                     & \!\!NCP$_i$--NCP$_{i+2}$ \!\!\!\!&\!\!\!\! 171.2 \!\!\!\!& $\!\!\!\!\!\!\pm 76.6$ \AA\, \!\!\!\!\!\!\!\!& $24.7^\circ$ \!\!\!\!\!& $\!\!\!\!\!\!\pm 21.8^\circ$ \\ \cmidrule{2-6}
        \multirow{3}{*}{\rotatebox{90}{\tiny 1 mM Mg$^{2+}$\!\!\!}} & \!\!NCP$_1$--NCP$_3$     \!\!\!\!&\!\!\!\!  67.6 \!\!\!\!& $\!\!\!\!\!\!\pm 3.5$  \AA\, \!\!\!\!\!\!\!\!& $22.5^\circ$ \!\!\!\!\!& $\!\!\!\!\!\!\pm 5.3^\circ$  \\
        ~                                                     & \!\!NCP$_2$--NCP$_4$     \!\!\!\!&\!\!\!\!  64.1 \!\!\!\!& $\!\!\!\!\!\!\pm 4.0$  \AA\, \!\!\!\!\!\!\!\!& $20.3^\circ$ \!\!\!\!\!& $\!\!\!\!\!\!\pm 3.3^\circ$  \\ 
        ~                                                     & \!\!NCP$_i$--NCP$_{i+2}$ \!\!\!\!&\!\!\!\!  \alert{65.9} \!\!\!\!& \alert{$\!\!\!\!\!\!\pm 4.1$  \AA\,} \!\!\!\!\!\!\!\!& \alert{$21.4^\circ$} \!\!\!\!\!& \alert{$\!\!\!\!\!\!\pm 4.6^\circ$}  \\ \cmidrule{2-6}
        \multirow{3}{*}{\rotatebox{90}{\tiny 1ZBB}}           & \!\!NCP$_1$--NCP$_3$     \!\!\!\!&\!\!\!\!  57.4 \!\!\!\!& \!\!\!\!\AA\,                \!\!\!\!\!\!\!\!& $11.2^\circ$ \!\!\!\!\!&                              \\
        ~                                                     & \!\!NCP$_2$--NCP$_4$     \!\!\!\!&\!\!\!\!  57.5 \!\!\!\!& \!\!\!\!\AA\,                \!\!\!\!\!\!\!\!& $11.6^\circ$ \!\!\!\!\!&                              \\ 
        ~                                                     & \!\!NCP$_i$--NCP$_{i+2}$ \!\!\!\!&\!\!\!\!  57.4 \!\!\!\!& $\!\!\!\!\!\!\pm 0.1$  \AA\, \!\!\!\!\!\!\!\!& $11.4^\circ$ \!\!\!\!\!& $\!\!\!\!\!\!\pm 0.2^\circ$  \\ \cmidrule{2-6}
      \end{tabular}
      
      \begin{tabular}{ l l  r l  r l  }
        \multicolumn{2}{c}{}                                                                    & \multicolumn{2}{c}{\shortstack[1]{\tiny Separation\\\tiny Distance}}        & \multicolumn{2}{c}{\shortstack[1]{\tiny Twist\\\tiny Angle}} \\[-1.2mm] \cmidrule{2-6}
        % \multicolumn{2}{c}{}                                                                    & \multicolumn{2}{c}{\shortstack[1]{Separation\\ Distance}}        & \multicolumn{2}{c}{\shortstack[1]{Twist\\ Angle}} \\[-1.2mm] \cmidrule{2-6}
        % \multirow{4}{*}{\rotatebox{90}{\tiny 10 mM K$^+$}}    & \!\!NCP$_1$--NCP$_2$     \!\!\!\!&\!\!\!\! 150.8 \!\!\!\!& $\!\!\!\!\!\!\pm 4.4$  \AA\, \!\!\!\!\!\!\!\!\!\!\!& $26.8^\circ $ \!\!\!\!\!& $\!\!\!\!\!\!\pm 6.2^\circ$  \\ 
        % ~                                                     & \!\!NCP$_2$--NCP$_3$     \!\!\!\!&\!\!\!\! 162.9 \!\!\!\!& $\!\!\!\!\!\!\pm 3.4$  \AA\, \!\!\!\!\!\!\!\!\!\!\!& $43.7^\circ $ \!\!\!\!\!& $\!\!\!\!\!\!\pm 6.2^\circ$  \\ 
        % ~                                                     & \!\!NCP$_3$--NCP$_4$     \!\!\!\!&\!\!\!\! 208.4 \!\!\!\!& $\!\!\!\!\!\!\pm 9.6$  \AA\, \!\!\!\!\!\!\!\!\!\!\!& $76.8^\circ $ \!\!\!\!\!& $\!\!\!\!\!\!\pm 65.9^\circ$ \\ 
        % ~                                                     & \!\!NCP$_i$--NCP$_{i+1}$ \!\!\!\!&\!\!\!\! 174.0 \!\!\!\!& $\!\!\!\!\!\!\pm 25.6$ \AA\, \!\!\!\!\!\!\!\!\!\!\!& $49.1^\circ $ \!\!\!\!\!& $\!\!\!\!\!\!\pm 43.6^\circ$ \\ \cmidrule{2-6} 
        \multirow{4}{*}{\rotatebox{90}{\tiny 1 mM Mg$^{2+}$\!\!\!}} & \!\!NCP$_1$--NCP$_2$     \!\!\!\!&\!\!\!\! 143.0 \!\!\!\!& $\!\!\!\!\!\!\pm 2.2$  \AA\, \!\!\!\!\!\!\!\!\!\!\!& $90.3^\circ $ \!\!\!\!\!& $\!\!\!\!\!\!\pm 3.9^\circ$  \\ 
        ~                                                     & \!\!NCP$_2$--NCP$_3$     \!\!\!\!&\!\!\!\! 136.3 \!\!\!\!& $\!\!\!\!\!\!\pm 1.5$  \AA\, \!\!\!\!\!\!\!\!\!\!\!& \alert{$100.2^\circ$} \!\!\!\!\!& \alert{$\!\!\!\!\!\!\pm 4.9^\circ$}  \\
        ~                                                     & \!\!NCP$_3$--NCP$_4$     \!\!\!\!&\!\!\!\! 145.9 \!\!\!\!& $\!\!\!\!\!\!\pm 3.1$  \AA\, \!\!\!\!\!\!\!\!\!\!\!& \alert{$98.0^\circ $} \!\!\!\!\!& \alert{$\!\!\!\!\!\!\pm 4.5^\circ$}  \\
        ~                                                     & \!\!NCP$_i$--NCP$_{i+1}$ \!\!\!\!&\!\!\!\! \alert{141.7} \!\!\!\!& \alert{$\!\!\!\!\!\!\pm 4.7$  \AA\,} \!\!\!\!\!\!\!\!\!\!\!& $96.2^\circ $ \!\!\!\!\!& $\!\!\!\!\!\!\pm 6.1^\circ$  \\ \cmidrule{2-6} 
        \multirow{4}{*}{\rotatebox{90}{\tiny 1ZBB}}           & \!\!NCP$_1$--NCP$_2$     \!\!\!\!&\!\!\!\! 147.6 \!\!\!\!& \!\!\!\!\AA\,                \!\!\!\!\!\!\!\!\!\!\!& $91.7^\circ $ \!\!\!\!\!&               \\                  
        ~                                                     & \!\!NCP$_2$--NCP$_3$     \!\!\!\!&\!\!\!\! 142.4 \!\!\!\!& \!\!\!\!\AA\,                \!\!\!\!\!\!\!\!\!\!\!& $93.0^\circ $ \!\!\!\!\!&               \\                  
        ~                                                     & \!\!NCP$_3$--NCP$_4$     \!\!\!\!&\!\!\!\! 147.5 \!\!\!\!& \!\!\!\!\AA\,                \!\!\!\!\!\!\!\!\!\!\!& $92.4^\circ $ \!\!\!\!\!&               \\                  
        ~                                                     & \!\!NCP$_i$--NCP$_{i+1}$ \!\!\!\!&\!\!\!\! 145.8 \!\!\!\!& $\!\!\!\!\!\!\pm 2.5$  \AA\, \!\!\!\!\!\!\!\!\!\!\!& $92.4^\circ $ \!\!\!\!\!& $\!\!\!\!\!\!\pm 0.5^\circ$  \\ \cmidrule{2-6}
      \end{tabular}
    }
    \column{0.4\paperwidth}
    \only<1>{
      \includegraphics[width=\textwidth]{figures/array_models/4x167_h5_mg1/den_all_1234}
    }
    \only<2>{
      \includemedia[
        width=\textwidth,
        height=0.75\textwidth,
        activate=pageopen,
        addresource=videos/4x167_h5_mg1_min1000.mp4,
        flashvars={
          source=videos/4x167_h5_mg1_min1000.mp4
          &loop=true                       % loop video
        }
      ]{\includegraphics[width=\textwidth]{figures/array_models/4x167_h5_mg1/den_all_1234}}{VPlayer.swf}
    }
    \includegraphics[width=\textwidth]{figures/array_models/1zbb_dual}
  \end{columns}
\end{frame}

\section*{}
%\subsection{Conclusions}
\begin{frame}{Conclusions}
  \vspace{-3mm}
  \begin{center}
    {\color{white} 
      How does the structure of nucleosomes in solution \\ 
      differ from the crystal structures?\\}
  \end{center}
  % \hspace{5mm}
  \begin{itemize}
    \onslide<1->{
    \item Measured nucleosome arrays using SAXS 
      \vspace{-5mm}
      \begin{multicols}{2}
        \begin{itemize}
        \item 1$\times$147
        \item 2$\times$167
        \item 3$\times$167
        \item 4$\times$167 (shown)
        \item 4$\times$167 gH5 (shown)
        \item 12$\times$167
        \end{itemize}
      \end{multicols}
    }
    \onslide<2->{
    \item Developed a CG simulation algorithm for dsDNA
      \begin{itemize}
      \item Metropolis MC sampling of a Markov Chain 
      \item Reproduces dsDNA bend and twist energetics 
      \item Generates models with atomic detail
      \end{itemize}
    }
    \onslide<3->{
    \item Filtered ensembles of atomic structures against SAXS data
    % }
    % \onslide<4->{
    \item Identified specific differences between the 1ZBB crystal and 1 mM Mg$^{2+}$ solution ensemble
    }
  \end{itemize}
\end{frame}


%\subsection{Acknowledgments}
\begin{frame}{Acknowledgments}
  \begin{columns}
    \column{.4\paperwidth}
    % \adjincludegraphics[width=\textwidth,trim={0 0 0 {0.104167\width}}, clip]{figures/1211chess}
    \adjincludegraphics[height=3cm,trim={{0.18\width} {0.07\width} {0.1\width} {0.183\width}}, clip]{figures/1211chess}
    \column{.4\paperwidth}
    \includegraphics[height=3cm]{figures/2014-11-05_xqiu_group}
    \column{.1\paperwidth}
    \includegraphics[height=1.5cm]{figures/carina_lorenzen}\\
    \includegraphics[height=1.45cm]{figures/suzanne_arnott}
  \end{columns}
  
  \begin{columns}
    \column{.95\paperwidth}
    
    \begin{itemize}
    \item Cornell High Energy Synchrotron Source: NSF DMR-1332208
    \item National Synchrotron Light Source: DOE DE-AC02-98CH10886
    \item \href{http://ccpsas.org/}{CCP-SAS}: EPSRC (EP/K039121/1) and NSF (CHE-1265821)
    \end{itemize}
    
  \end{columns}  
\end{frame}

\begin{frame}{}
  % \includegraphics[width=\textwidth]{figures/dna_xkcd}\\
  \includegraphics[width=\textwidth]{figures/dna_xkcd_orig}\\
  \tiny{\url{http://xkcd.com/1605/}}
\end{frame}

\begin{frame}{DNA unwinding observed in 10 mM K$^+$ ensemble.}
  \begin{columns}
    %\column{\dimexpr\paperwidth-12pt}
    \column{0.5\paperwidth}
    \onslide<2>{
      \renewcommand{\arraystretch}{1.2}
      \scriptsize
      \begin{tabular}{  r l  r l  r l }
        \multicolumn{2}{c}{}                                                                     & \multicolumn{2}{c}{\shortstack[1]{\tiny Stack\\\tiny Distance}}        & \multicolumn{2}{c}{\shortstack[1]{\tiny Angle Between\\\tiny Cylinder Axes}} \\[-1.2mm] \cmidrule{2-6}
        % \multicolumn{2}{c}{}                                                                     & \multicolumn{2}{c}{\shortstack[1]{ Stack\\ Distance}}        & \multicolumn{2}{c}{\shortstack[1]{ Angle Between\\ Cylinder Axes}} \\[-1.2mm] \cmidrule{2-6}
        \multirow{3}{*}{\rotatebox{90}{\tiny 10 mM K$^+$\!\!\!}}    & \!\!NCP$_1$--NCP$_3$     \!\!\!\!&\!\!\!\!  95.2 \!\!\!\!& $\!\!\!\!\!\!\pm 2.5$  \AA\, \!\!\!\!\!\!\!\!& \alert{$10.9^\circ$} \!\!\!\!\!& \alert{$\!\!\!\!\!\!\pm 3.9^\circ$}  \\
        ~                                                     & \!\!NCP$_2$--NCP$_4$     \!\!\!\!&\!\!\!\! 247.3 \!\!\!\!& $\!\!\!\!\!\!\pm 12.9$ \AA\, \!\!\!\!\!\!\!\!& $38.5^\circ$ \!\!\!\!\!& $\!\!\!\!\!\!\pm 23.5^\circ$ \\
        ~                                                     & \!\!NCP$_i$--NCP$_{i+2}$ \!\!\!\!&\!\!\!\! 171.2 \!\!\!\!& $\!\!\!\!\!\!\pm 76.6$ \AA\, \!\!\!\!\!\!\!\!& $24.7^\circ$ \!\!\!\!\!& $\!\!\!\!\!\!\pm 21.8^\circ$ \\ \cmidrule{2-6}
        % \multirow{3}{*}{\rotatebox{90}{\tiny 1 mM Mg$^{2+}$}} & \!\!NCP$_1$--NCP$_3$     \!\!\!\!&\!\!\!\!  67.6 \!\!\!\!& $\!\!\!\!\!\!\pm 3.5$  \AA\, \!\!\!\!\!\!\!\!& $22.5^\circ$ \!\!\!\!\!& $\!\!\!\!\!\!\pm 5.3^\circ$  \\
        % ~                                                     & \!\!NCP$_2$--NCP$_4$     \!\!\!\!&\!\!\!\!  64.1 \!\!\!\!& $\!\!\!\!\!\!\pm 4.0$  \AA\, \!\!\!\!\!\!\!\!& $20.3^\circ$ \!\!\!\!\!& $\!\!\!\!\!\!\pm 3.3^\circ$  \\ 
        % ~                                                     & \!\!NCP$_i$--NCP$_{i+2}$ \!\!\!\!&\!\!\!\!  65.9 \!\!\!\!& $\!\!\!\!\!\!\pm 4.1$  \AA\, \!\!\!\!\!\!\!\!& $21.4^\circ$ \!\!\!\!\!& $\!\!\!\!\!\!\pm 4.6^\circ$  \\ \cmidrule{2-6}
        \multirow{3}{*}{\rotatebox{90}{\tiny 1ZBB}}           & \!\!NCP$_1$--NCP$_3$     \!\!\!\!&\!\!\!\!  57.4 \!\!\!\!& \!\!\!\!\AA\,                \!\!\!\!\!\!\!\!& $11.2^\circ$ \!\!\!\!\!&                              \\
        ~                                                     & \!\!NCP$_2$--NCP$_4$     \!\!\!\!&\!\!\!\!  57.5 \!\!\!\!& \!\!\!\!\AA\,                \!\!\!\!\!\!\!\!& $11.6^\circ$ \!\!\!\!\!&                              \\ 
        ~                                                     & \!\!NCP$_i$--NCP$_{i+2}$ \!\!\!\!&\!\!\!\!  57.4 \!\!\!\!& $\!\!\!\!\!\!\pm 0.1$  \AA\, \!\!\!\!\!\!\!\!& $11.4^\circ$ \!\!\!\!\!& $\!\!\!\!\!\!\pm 0.2^\circ$  \\ \cmidrule{2-6}
      \end{tabular}
      
      \begin{tabular}{ l l  r l  r l  }
        \multicolumn{2}{c}{}                                                                    & \multicolumn{2}{c}{\shortstack[1]{\tiny Separation\\\tiny Distance}}        & \multicolumn{2}{c}{\shortstack[1]{\tiny Twist\\\tiny Angle}} \\[-1.2mm] \cmidrule{2-6}
        % \multicolumn{2}{c}{}                                                                    & \multicolumn{2}{c}{\shortstack[1]{Separation\\ Distance}}        & \multicolumn{2}{c}{\shortstack[1]{Twist\\ Angle}} \\[-1.2mm] \cmidrule{2-6}
        \multirow{4}{*}{\rotatebox{90}{\tiny 10 mM K$^+$}\!\!\!}    & \!\!NCP$_1$--NCP$_2$     \!\!\!\!&\!\!\!\! 150.8 \!\!\!\!& $\!\!\!\!\!\!\pm 4.4$  \AA\, \!\!\!\!\!\!\!\!\!& \alert{$26.8^\circ $} \!\!\!\!\!& \alert{$\!\!\!\!\!\!\pm 6.2^\circ$}  \\ 
        ~                                                     & \!\!NCP$_2$--NCP$_3$     \!\!\!\!&\!\!\!\! 162.9 \!\!\!\!& $\!\!\!\!\!\!\pm 3.4$  \AA\, \!\!\!\!\!\!\!\!\!& \alert{$43.7^\circ $} \!\!\!\!\!& \alert{$\!\!\!\!\!\!\pm 6.2^\circ$}  \\ 
        ~                                                     & \!\!NCP$_3$--NCP$_4$     \!\!\!\!&\!\!\!\! \alert{208.4} \!\!\!\!& \alert{$\!\!\!\!\!\!\pm 9.6$  \AA\,} \!\!\!\!\!\!\!\!\!& $76.8^\circ $ \!\!\!\!\!& $\!\!\!\!\!\!\pm 65.9^\circ$ \\ 
        ~                                                     & \!\!NCP$_i$--NCP$_{i+1}$ \!\!\!\!&\!\!\!\! \alert{174.0} \!\!\!\!& \alert{$\!\!\!\!\!\!\pm 25.6$ \AA\,} \!\!\!\!\!\!\!\!\!& $49.1^\circ $ \!\!\!\!\!& $\!\!\!\!\!\!\pm 43.6^\circ$ \\ \cmidrule{2-6} 
        % \multirow{4}{*}{\rotatebox{90}{\tiny 1 mM Mg$^{2+}$}} & \!\!NCP$_1$--NCP$_2$     \!\!\!\!&\!\!\!\! 143.0 \!\!\!\!& $\!\!\!\!\!\!\pm 2.2$  \AA\, \!\!\!\!\!\!\!\!\!\!\!& $90.3^\circ $ \!\!\!\!\!& $\!\!\!\!\!\!\pm 3.9^\circ$  \\ 
        % ~                                                     & \!\!NCP$_2$--NCP$_3$     \!\!\!\!&\!\!\!\! 136.3 \!\!\!\!& $\!\!\!\!\!\!\pm 1.5$  \AA\, \!\!\!\!\!\!\!\!\!\!\!& $100.2^\circ$ \!\!\!\!\!& $\!\!\!\!\!\!\pm 4.9^\circ$  \\
        % ~                                                     & \!\!NCP$_3$--NCP$_4$     \!\!\!\!&\!\!\!\! 145.9 \!\!\!\!& $\!\!\!\!\!\!\pm 3.1$  \AA\, \!\!\!\!\!\!\!\!\!\!\!& $98.0^\circ $ \!\!\!\!\!& $\!\!\!\!\!\!\pm 4.5^\circ$  \\
        % ~                                                     & \!\!NCP$_i$--NCP$_{i+1}$ \!\!\!\!&\!\!\!\! 141.7 \!\!\!\!& $\!\!\!\!\!\!\pm 4.7$  \AA\, \!\!\!\!\!\!\!\!\!\!\!& $96.2^\circ $ \!\!\!\!\!& $\!\!\!\!\!\!\pm 6.1^\circ$  \\ \cmidrule{2-6} 
        \multirow{4}{*}{\rotatebox{90}{\tiny 1ZBB}}           & \!\!NCP$_1$--NCP$_2$     \!\!\!\!&\!\!\!\! 147.6 \!\!\!\!& \!\!\!\!\AA\,                \!\!\!\!\!\!\!\!\!& $91.7^\circ $ \!\!\!\!\!&               \\                  
        ~                                                     & \!\!NCP$_2$--NCP$_3$     \!\!\!\!&\!\!\!\! 142.4 \!\!\!\!& \!\!\!\!\AA\,                \!\!\!\!\!\!\!\!\!& $93.0^\circ $ \!\!\!\!\!&               \\                  
        ~                                                     & \!\!NCP$_3$--NCP$_4$     \!\!\!\!&\!\!\!\! 147.5 \!\!\!\!& \!\!\!\!\AA\,                \!\!\!\!\!\!\!\!\!& $92.4^\circ $ \!\!\!\!\!&               \\                  
        ~                                                     & \!\!NCP$_i$--NCP$_{i+1}$ \!\!\!\!&\!\!\!\! 145.8 \!\!\!\!& $\!\!\!\!\!\!\pm 2.5$  \AA\, \!\!\!\!\!\!\!\!\!& $92.4^\circ $ \!\!\!\!\!& $\!\!\!\!\!\!\pm 0.5^\circ$  \\ \cmidrule{2-6}
      \end{tabular}
    }
    \column{0.4\paperwidth}
    \only<1>{
      \includegraphics[width=\textwidth]{figures/array_models/4x167_h5_k010/den_all_1234}
    }
    \only<2>{
      \includemedia[
        width=\textwidth,
        height=0.75\textwidth,
        activate=pageopen,
        addresource=videos/4x167_h5_k010_min1000.mp4,
        flashvars={
          source=videos/4x167_h5_k010_min1000.mp4
          &loop=true                       % loop video
        }
      ]{\includegraphics[width=\textwidth]{figures/array_models/4x167_h5_k010/den_all_1234}}{VPlayer.swf}
    }
    \includegraphics[width=\textwidth]{figures/array_models/1zbb_dual}
  \end{columns}
\end{frame}

\begin{frame}{}
  \begin{center}
    \textit{``If our small minds, for some convenience, divide this \dots universe, into parts---physics, biology, geology, astronomy, psychology, and so on---remember that nature does not know it!''}\\
    \textit{\hspace{5cm} - Richard P. Feynman, 1963}\\
  \end{center}
\end{frame}

\end{document}
