% $Id$
\newpage
%\newgeometry{top=1in, bottom=1in, left=1.25in, right=1in}
%\section{\protect Appendices}\label{chp:app}
\section{Appendices}\label{chp:app}
\doublespacing
\subsection{A: Supplemental data for ``Elucidating Internucleosome Interactions and the Roles of Histone Tails}\label{app:mono}
Steven C.~Howell,$^\dagger$ Kurt Andresen,$^\ddagger$ Isabel Jimenez-Useche,$^\S$, Chongli Yuan, $^\S$ and Xiangyun Qiu$^\dagger$

\noindent
$^\dagger$Department of Physics, George Washington University, Washington, DC;\\
$^\ddagger$Department of Physics, Gettysburg College, Gettysburg, Pennsylvania; \\
$^\S$School of Chemical Engineering, Purdue University, West Lafayette, Indiana

\noindent
Biophysical Journal\quad Volume 105\quad July 2013\quad 194-199

\newpage
\subsubsection{Supporting Data to the Results Presented in the Main Text}
\begin{figure}[!htb]
  \centering
  \begin{subfigure}[b]{0.24\textwidth}
    \includegraphics[width=\textwidth]{figures/mono/sfig1a}
      \caption{PAGE of DNA}
      \label{fig:mono:s1a}
  \end{subfigure}
  \begin{subfigure}[b]{0.30\textwidth}
    \includegraphics[width=\textwidth]{figures/mono/sfig1b}
      \caption{SDS-PAGE of NS-NCP}
      \label{fig:mono:s1b}
  \end{subfigure}
  \begin{subfigure}[b]{0.42\textwidth}
    \includegraphics[width=\textwidth]{figures/mono/sfig1c}
      \caption{SDS-PAGE of RC-NCPs}
      \label{fig:mono:s1c}
  \end{subfigure}
  \caption[Characterization of NCP Samples]{\label{fig:mono:s1}
    \textbf{Characterization of Nucleosome Samples:}
    (i) DNA extracted from NS-NCPs, ``NS'', and the recombinant 147 bp MMTV DNA, ``RC'', in 5\% PAGE.
    The ``M'' lane is the 100 bp DNA marker (Invitrogen). 
    By analyzing the intensity distribution of each band, we obtained the full widths at half maximum (FWHM) of the bands as, 100 bp marker: 10 pixels, 200 bp marker: 7.5 pixels, NS-DNA: 15 pixels, RC-DNA: 10.5 pixels. 
    Given the distance between 100 bp and 200 bp markers are \textapprox100 pixels, the additional smearing of the NS-DNA band is estimated to be \textapprox10 pixels in FWHM, corresponding to $\pm$5 bp.
    The maximum peak positions for NS-DNA and RC-DNA are identical within experimental error, noting that the NS-DNA band intensity distribution is skewed toward longer DNA lengths as expected from the nucleosomal hindrance of Micrococcal nuclease digestion. 
    We thus estimated the NS-DNA to be $147\pm\SI{5}{bp}$. 
    (ii) SDS-PAGE (15\%) of natural-source NCP histones. 
    The ``M'' lane is the protein marker (Invitrogen BenchMark\texttrademark). 
    (iii) SDS-PAGE of recombinant NCP histones (``intact'' refers to the RC-NCP with all histone tails). 
    Note that the recombinant H2A and H2B histones migrate as one band, while the H2A and H2B histones from NS-NCPs are well separated (ii). 
    Possible explanations include sequence differences and post-translational modifications in NS-NCP histones only.
  }
\end{figure}

\begin{figure}[!htb]
  \centerline{\includegraphics[width=\textwidth]{figures/mono/sfig2}}
  \caption[Form Factors the Mononucleosomes Constructs]{\label{fig:mono:s2}
    \textbf{The Form Factors for the Nucleosome Constructs in this Study:}
    The SAXS profiles are calculated based on the nucleosome crystal structure (1KX5) using Crysol \citep{svergun_crysol_1995}. 
    Atomic structures for the histone-tail-deletion constructs (i.e., gH3 and gH4 NCPs) are obtained by manual removal of the residues in accord with the constructs. 
    The wild-type curve is used as the form factor for the NS-NCP and RC-NCP (results from the latter are shown in \cref{fig:mono:s5}). 
    Comparisons of the three form factors indicate that tail deletions lead to rather small changes to the shape of the SAXS profile.
  }
\end{figure}

\begin{figure}[!htb]
  \centerline{\includegraphics[width=\textwidth]{figures/mono/sfig3}}
  \caption[SAXS Profiles of NS-NCPs in a Series of MgCl$_2$ Concentrations]{\label{fig:mono:s3}
    \textbf{SAXS Profiles of NS-NCPs in a Series of MgCl$_2$ Concentrations (no theoretical fits):}
    Inter-NCP repulsion (low Q downturn) was observed at 0.5 and \SI{1.0}{mM} MgCl$_2$, while attraction (low Q upturn) were observed at 2.0 and \SI{3.0}{mM} MgCl$_2$. 
    We have not been able to obtain satisfactory fits to the full SAXS profiles in Mg$^{2+}$ salts using the GOCM. 
    We do not yet know the reason. 
    One speculation is that the short-range inter-NCP interactions become strongly anisotropic in Mg$^{2+}$ salts and the GOCM fails, or that there exist oligomeric states of NCPs in the presence of Mg$^{2+}$. 
    Still, the qualitative observations of inter-NCP interactions (i.e., repulsion versus attraction) hold. 
    Quantitative modeling of the SAXS data in Mg$^{2+}$ salts is being actively pursued in our groups.
  }
\end{figure}

\begin{figure}[!htb]
  \centerline{\includegraphics[width=\textwidth]{figures/mono/sfig4}} 
  \caption[SAXS Profiles and GOCM Fits of gH3 and gH4 RC-NCPs in 20 and \SI{40}{mM} KCl]{\label{fig:mono:s4}
    \textbf{SAXS Profiles and GOCM Fits of gH3 and gH4 RC-NCPs in 20 and 40 mM KCl:}
    I(Q)s are normalized by NCP concentrations to assist visual comparison. 
    Each panel shows experimental I(Q)s (symbols) at a series of [NCP]s as indicated in the legends, together with their respective theoretical fits (lines). 
    The residues are shown with an offset at the same scale.
    The pertinent pair-potential parameters are (discussed in detail in \hyperref[sec:mono:results]{chapter 2}), (a) $Z_\text{eff}$ = 22(1) e, $\sigma_{\!\!_{N}} = \SI{130}{\angstrom}$; (b) $Z_\text{eff} = \SI{21(1)}{\textit{e}}$, $\sigma_{\!\!_{N}} = \SI{110}{\angstrom}$; (c) $Z_\text{eff} = \SI{23(1)}{\textit{e}}$, $\sigma_{\!\!_{N}} = \SI{130}{\angstrom}$; (d) unreliable fitted values due to the relative low [NCP]s and thus not given.
  }
\end{figure}

\begin{figure}[!htb]
  \centerline{\includegraphics[width=\textwidth]{figures/mono/sfig5}} 
  \caption[SAXS Profiles and GOCM fits of Intact Recombinant RC-NCPs in \SI{10}{mM} KCl]{\label{fig:mono:s5}
    \textbf{SAXS Profiles and GOCM fits of Intact Recombinant RC-NCPs in 10 mM KCl:}
    Annotations follow that of \cref{fig:mono:s4}, namely: 
    I(Q)s are normalized by NCP concentrations to assist visual comparison. 
    Experimental I(Q)s (symbols) are shown at a series of [NCP]s as indicated in the legend, together with their respective theoretical fits (lines). 
    The residues are shown with an offset at the same scale.
    The pair-potential parameters are, $Z_\text{eff}$ =21(1) e, $\sigma_{\!\!_{N}}$=140 \AA.
  }
\end{figure}

\clearpage

\subsubsection{Modeling the Structure Factor with a Two-DH Inter-NCP Interaction Potential}
In addition to the use of either repulsive or attractive DH potential to fit the structure factor as in the main text, the inclusion of both DH terms in the inter-NCP potential was presented here. 
This potential takes the form shown in below (the same as \cref{eqn:mono:dh} in \hyperref[sec:mono:results]{Chapter 2}),
\begin{align}\label{eqn:sup:dh}
  U(r) = \left\{ 
  \begin{array}{l l}
    \infty & \quad r<\sigma_{\!\!_{N}}\\
    % \displaystyle\frac{Z_\text{eff}^2}{\epsilon\left(1+\left.\kappa_D\,\sigma_{\!\!_{N}}\right/2\right)^2}\frac{e^{-\kappa_D\left(r-\sigma_{\!\!_{N}}\right)}}{r}-\frac{Z_\text{attr}^2}{\epsilon\left(1+\left.\kappa_\text{attr}\,\sigma_{\!\!_{N}}\right/2\right)^2}\frac{e^{-\kappa_\text{attr}\left(r-\sigma_{\!\!_{N}}\right)}}{r} & \quad r\geq\sigma_{\!\!_{N}}
    \dfrac{Z_\text{eff}^2}{\epsilon\left(1+\frac{\kappa_D\,\sigma_{\!\!_{N}}}{2}\right)^2}\dfrac{e^{-\kappa_D\left(r-\sigma_{\!\!_{N}}\right)}}{r}-\dfrac{Z_\text{attr}^2}{\epsilon\left(1+\frac{\kappa_\text{attr}\,\sigma_{\!\!_{N}}}{2}\right)^2}\dfrac{e^{-\kappa_\text{attr}\left(r-\sigma_{\!\!_{N}}\right)}}{r} & \quad r\geq\sigma_{\!\!_{N}}
  \end{array} \right.\,,
\end{align}
Here the hard-core potential has a fixed diameter of $\sigma_{\!\!_{N}}$=100 \AA~for all samples; the electrostatic repulsive DH term has Debye length (1/$\kappa_D$) given by the ionic strength; and the attractive DH has decay length (1/$\kappa_\text{attr}$) fixed as 4.8 \AA, equivalent to the Debye length at \SI{400}{mM} monovalent salt.
It should be noted that, as we do not know the exact nature of the inter-NCP attraction, the choice of a DH potential for inter-NCP attraction is convenient due to the available computational methods \citep{liu_cluster_2005} but somewhat arbitrary. 
The choice of 4.8 \AA~Debye length is due to the most likely short-range nature of inter-NCP attraction, the observation of inter-molecular attractions of 4.8 \AA~decay length between various types of molecular surfaces \citep{zemb_editorial_2011}, and the necessity of significant differences from the Debye length of the repulsive DH term in salt conditions from 10 to \SI{200}{mM} KCl.

This leaves two fitting parameters in the inter-NCP potential, the effective charges of the repulsive ($Z_\text{eff}$ ) and attractive ($Z_\text{attr}$ ) DH terms. 
However, one complication is that there exists significant correlation between the two fitting parameters, particularly at high salts when their Debye lengths are close. 
This is because of the mutual cancellation effects of the two potentials.
For this reason, while excellent fits were obtained (not shown), correlations between $Z_\text{eff}$ and $Z_\text{attr}$ values were evident from their fluctuations from sample to sample. 
To avoid such large fluctuations and facilitate comparisons between different salt conditions, we made one assumption that the attractive potential for each NCP construct (i.e., NS, gH3, or gH4 NCP) is independent of ion condition, i.e., the same $Z_\text{attr}$ used for all salt conditions. 
The $Z_\text{attr}$ value for each construct was determined by averaging the fitted $Z_\text{attr}$ values at different salts for the particular construct. 
This procedure led to decreases of the fitting qualities, though rather satisfactory agreements with experimental data were obtained for all samples.

We show the fitted results based on the procedure discussed above in \cref{fig:mono:s6,fig:mono:s7,fig:mono:s8,fig:mono:s9} and the fitted potential parameters in \cref{tab:mono:Z}. 
Compared with the fits with one DH-term potential discussed in the main text, the decrease of fitting quality is more significant for the more concentrated NCP samples. 
As the attractive DH term is present at all salt conditions, the fitted repulsive effective charges, $Z_\text{eff}$, are larger than those obtained with the one-DH repulsive potential as expected. 
The effective charges ($<\SI{50}{\textit{e}}$) are still substantially smaller than the bare charge (\textapprox\SI{150}{\textit{e}}) or the theoretical renormalized charges ($>\SI{100}{\textit{e}}$). 
Qualitative comparisons are thus unchanged, while the fitted inter-NCP potentials are changed quantitatively as given in \cref{tab:mono:Z}. 
For the attractive DH term, this fitting procedure obtains the effective charge $Z_\text{attr}$ of 200, 165, and \SI{154}{\textit{e}} for the NS, gH3, and gH4 NCPs respectively.
This is consistent with the results from one-DH potential fits indicating decreased attraction with tail deletions.

Overall, the inclusion of a persisting attractive term is intellectually pleasing as inter-NCP attraction (e.g., mediated by histone tails) is very likely present at all times, though overwhelmed by electrostatic repulsion at low salts. 
However, simultaneous determinations of both repulsive and attractive contributions are challenging.

\begin{figure}[!htb]
  \centerline{\includegraphics[width=\textwidth]{figures/mono/sfig6}} 
  \caption[SAXS profiles of NS-NCPs in a Series of KCl concentrations with fits based on the two-DH inter-NCP potential]{\label{fig:mono:s6}
    \textbf{SAXS profiles of NS-NCPs in a Series of KCl concentrations with fits based on the two-DH inter-NCP potential:} 
    Annotations are the same as in \cref{fig:mono:2} in the main text, namely:
    I(Q)s are normalized by NCP concentrations to assist comparison.
    Each panel shows experimental I(Q)s (\textit{symbols}) at a series of [NCP]s (\SI{0.1}{mM} gives \textapprox\SI{20}{mg/mL}) as indicated in the legends, together with their respective theoretical fits (\textit{lines}). 
    The residues are shown with an offset at the same scale. 
  }
\end{figure}

\begin{figure}[!htb]
  \centerline{\includegraphics[width=\textwidth]{figures/mono/sfig7}} 
  \caption[SAXS profiles of gH3 and gH4 NCPs in 10 and \SI{100}{mM} KCl with fits based on the two-DH inter-NCP potential]{\label{fig:mono:s7}
    \textbf{SAXS profiles of gH3 and gH4 NCPs in 10 and 100 mM KCl with fits based on the two-DH inter-NCP potential:}
    Annotations follow that of \cref{fig:mono:2} in the main text, namely:
    I(Q)s are normalized by NCP concentrations to assist comparison.
    Each panel shows experimental I(Q)s (\textit{symbols}) at a series of [NCP]s (\SI{0.1}{mM} gives \textapprox\SI{20}{mg/mL}) as indicated in the legends, together with their respective theoretical fits (\textit{lines}). 
    The residues are shown with an offset at the same scale. 
  }
\end{figure}

\begin{figure}[!htb]
  \centerline{\includegraphics[width=\textwidth]{figures/mono/sfig8}} 
  \caption[SAXS profiles of gH3 and gH4 NCPs in 20 and \SI{40}{mM} KCl with fits based on the two-DH inter-NCP potential]{\label{fig:mono:s8}
    \textbf{SAXS profiles of gH3 and gH4 NCPs in 20 and 40 mM KCl with fits based on the two-DH inter-NCP potential:}
    Annotations follow that of \cref{fig:mono:2} in the main text, namely:
    I(Q)s are normalized by NCP concentrations to assist comparison.
    Each panel shows experimental I(Q)s (\textit{symbols}) at a series of [NCP]s (\SI{0.1}{mM} gives \textapprox\SI{20}{mg/mL}) as indicated in the legends, together with their respective theoretical fits (\textit{lines}). 
    The residues are shown with an offset at the same scale. 
  }
\end{figure}

\begin{figure}[!htb]
  \centerline{\includegraphics[width=\textwidth]{figures/mono/sfig9}} 
  \caption[SAXS profiles of RC-NCPs in \SI{10}{mM} KCl with fits based on the two-DH inter-NCP potential]{\label{fig:mono:s9}
    \textbf{SAXS profiles of RC-NCPs in 10 mM KCl with fits based on the two-DH inter-NCP potential:}
    Annotations follow that of \cref{fig:mono:2} in the main text, namely:
    I(Q)s are normalized by NCP concentrations to assist comparison.
    I(Q)s (\textit{symbols}) are shown at a series of [NCP]s (\SI{0.1}{mM} gives \textapprox\SI{20}{mg/mL}) as indicated in the legend, together with their respective theoretical fits (\textit{lines}). 
    The residues are shown with an offset at the same scale. 
    The pertinent pair-potential parameters (discussed in the \hyperref[eqn:mono:dh]{main text}) are shown in \cref{tab:mono:Z}.
  }
\end{figure}




