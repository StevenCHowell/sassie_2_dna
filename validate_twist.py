import numpy as np
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import double_stranded_nucleic_torsion as dsnt
import sassie.util.basis_to_python as basis_to_python
import sassie_0_dna.util.gw_plot as gwp
try:
    import sasmol
except:
    import sassie.sasmol.sasmol as sasmol

def plot_axes(all_twist_angles, all_bp_origins, all_bp_axes):
    # Visualize the first full DNA helix
    fig = plt.figure()
    ax = fig.add_subplot(211, projection='3d')
    plt.hold(True)
    for i in xrange(12):
        if i > 0:
            label_string = r'$\delta\omega_%d=%0.1f$' % (i, all_twist_angles[i-1,0])
        else:
            label_string = 'bp 0'
        x = np.vstack((all_bp_origins[i], all_bp_origins[i] + all_bp_axes[0, i]))
        ax.plot(x[:,0], x[:,1], x[:,2], 'r')
        y = np.vstack((all_bp_origins[i], all_bp_origins[i] + all_bp_axes[1, i]))
        ax.plot(y[:,0], y[:,1], y[:,2], 'g')
        z = np.vstack((all_bp_origins[i], all_bp_origins[i] + all_bp_axes[2, i]))
        ax.plot(z[:,0], z[:,1], z[:,2], 'b', label=label_string)

    # Shrink current axis by 20%
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

    # Put a legend to the right of the current axis
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    plt.axis('equal')
    ax.legend(loc='upper left', numpoints=1, bbox_to_anchor=(1, 0.5))
    # plt.show()


    # Compare the first two axes
    ax = fig.add_subplot(212, projection='3d')
    for i in xrange(2):
        if i > 0:
            label_string = r'$\delta\omega_%d=%0.1f$' % (i, all_twist_angles[i-1,0])
        else:
            label_string = 'bp 0'
        x = np.vstack((all_bp_origins[0], all_bp_origins[0] + all_bp_axes[0, i]))
        ax.plot(x[:,0], x[:,1], x[:,2], 'r')
        y = np.vstack((all_bp_origins[0], all_bp_origins[0] + all_bp_axes[1, i]))
        ax.plot(y[:,0], y[:,1], y[:,2], 'g')
        z = np.vstack((all_bp_origins[0], all_bp_origins[0] + all_bp_axes[2, i]))
        ax.plot(z[:,0], z[:,1], z[:,2], 'b', label=label_string)
    xlim = [all_bp_origins[0, 0] + 2, all_bp_origins[0, 0] - 2]
    ylim = [all_bp_origins[0, 1] + 2, all_bp_origins[0, 1] - 2]
    zlim = [all_bp_origins[0, 2] + 2, all_bp_origins[0, 2] - 2]
    plt.axis('equal')
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.set_zlim(zlim)

    # Shrink current axis by 20%
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

    # Put a legend to the right of the current axis
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    ax.legend(loc='upper left', numpoints=1, bbox_to_anchor=(1, 0.5))
    plt.show()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

pdb_file_name = 'c36_min_dsDNA60.pdb'
dcd_file_name = 'twist_1000/monte_carlo/c36_mc_dsDNA60.dcd'
n_init = 0
n_sample = 1

dcd_file_name = 'twist_1000/monte_carlo/c36_dna_1k_sparse.dcd'
n_init = 100
n_sample = 10

# dcd_file_name = 'twist_350k/monte_carlo/c36_dna_50k_sparse.dcd'
# dcd_file_name = 'twist_350k/monte_carlo/c36_dna_100k_sparse.dcd'
# dcd_file_name = 'twist_350k/monte_carlo/c36_dna_150k_sparse.dcd'
# dcd_file_name = 'twist_350k/monte_carlo/c36_dna_200k_sparse.dcd'
# dcd_file_name = 'twist_350k/monte_carlo/c36_dna_250k_sparse.dcd'
# dcd_file_name = 'twist_350k/monte_carlo/c36_dna_300k_sparse.dcd'
dcd_file_name = 'twist_350k/monte_carlo/c36_dna_350k_sparse.dcd'

output_file_name = dcd_file_name[:-4] + '.npy'

n_init = 1000
n_sample = 100

try:
    all_twist_angles = np.load(output_file_name)
except:

    # 0. Create molecule and load in structures
    dna = sasmol.SasMol(0)
    dna.read_pdb(pdb_file_name)
    dcd_file = dna.open_dcd_read(dcd_file_name)
    nf = dcd_file[2]

    bps = np.array([np.linspace(0, 60, 61), np.linspace(121, 61, 61)]).T
    n_bps = len(bps) - 1

    all_bp_mols = []
    all_bp_origins = np.zeros((n_bps, 3))
    all_bp_axes = np.zeros((3, n_bps, 3))
    all_bp_atom_masks = []
    all_bp_masks = []
    segnames = ['DNA1', 'DNA2']

    all_twist_angles = np.zeros((n_bps-1, nf+1))

    for i in xrange(n_bps):
        i_bp = i+1
        bp_mol = sasmol.SasMol(0)
        bp_filter = '(segname %s and resid %d) or (segname %s and resid %d)' % (
            segnames[0], bps[i_bp, 0], segnames[1], bps[i_bp, 1])
        error, bp_mask = dna.get_subset_mask(basis_to_python.parse_basis(bp_filter))
        assert not error, error
        error = dna.copy_molecule_using_mask(bp_mol, bp_mask, 0)
        assert not error, error
        bp_origin, bp_axes, bp_atom_masks = dsnt.get_dna_bp_reference_frame(
            segnames, bp_mol)

        all_bp_origins[i, :] = bp_origin
        all_bp_axes[:, i, :] = bp_axes
        all_bp_atom_masks.append(bp_atom_masks)
        all_bp_mols.append(bp_mol)
        all_bp_masks.append(bp_mask)

    all_twist_angles[:, 0] = dsnt.get_twist_angles(all_bp_axes)

    show = False
    if show:
        plot_axes(all_twist_angles, all_bp_origins, all_bp_axes)

    # 1. Iterate through the frames calculating the twist angles for each
    for j in xrange(nf):
        dna.read_dcd_step(dcd_file, j+1)
        for i in xrange(n_bps):
            i_bp = i+1
            bp_mol = all_bp_mols[i]
            error, bp_coor = dna.get_coor_using_mask(0, all_bp_masks[i])
            assert not error, error
            assert bp_coor.shape == bp_mol.coor().shape
            bp_mol.setCoor(bp_coor)
            (all_bp_origins[i, :], all_bp_axes[:, i, :], _
             ) = dsnt.get_dna_bp_reference_frame(segnames, bp_mol,
                                                 bp_masks=all_bp_atom_masks[i])

        all_twist_angles[:, j+1] = dsnt.get_twist_angles(all_bp_axes)

    dna.close_dcd_read(dcd_file[0])

    np.save(output_file_name, all_twist_angles)


# 2. Calculate and plot the average and each individual twist
#    angle for each step

mean_twist_angle = all_twist_angles.mean(axis=0)
std_twist_angle = all_twist_angles.std(axis=0)
fig = plt.figure() # figsize=(6, 5))
gs = gridspec.GridSpec(1, 1, left=0.1, right=0.9, wspace=0, hspace=0)

# http://www.pnas.org/content/95/19/11163.short
goal_mean = 35.4
goal_std = 6.3
# ylim = [goal_mean - 3 * goal_std, goal_mean + 3 * goal_std]
ylim = [0, 90]
_, n_frames = all_twist_angles.shape
ns = n_init + (n_frames -1) * n_sample
steps = np.arange(n_init, ns+1, n_sample, dtype=int)
goal_mean_array = np.ones(steps.shape) * goal_mean

ax = plt.subplot(gs[0])
for row in xrange(len(all_twist_angles)):
    ax.plot(steps, all_twist_angles[row,:], '.', ms=1, color='DimGray')
ax.plot(steps, mean_twist_angle, '-', c=gwp.qual_color(6), linewidth=1,
        label=r'$\overline{\omega}$')
ax.plot(steps, mean_twist_angle-(2*std_twist_angle), c=gwp.qual_color(0),
        linewidth=1, label=r'$\overline{\omega}\pm 2\sigma_{_{\!\!\!SD}}$')
ax.plot(steps, mean_twist_angle+(2*std_twist_angle), c=gwp.qual_color(0),
        linewidth=1)
ax.plot(steps, goal_mean_array, '--', c=gwp.qual_color(1), linewidth=2,
        label=r'$\overline{\omega}$ crystal')
ax.plot(steps, goal_mean_array+(2*goal_std), '--', c=gwp.qual_color(4),
        linewidth=2)
ax.plot(steps, goal_mean_array-(2*goal_std), '--', c=gwp.qual_color(4),
        linewidth=2, label=r'$\overline{\omega}\pm 2\sigma_{_{\!\!\!SD}}$ crystal')
lg = ax.legend(loc='upper left', numpoints=1)
lg.draw_frame(False)
# ax.set_title(r'DNA Twist Angle ($\omega$)')
ax.set_xlabel('MC Steps')
ax.set_ylabel('Twist Angle (degrees)')
# ax.ticklabel_format(axis='x', style = 'sci', useOffset=False, scilimits=(3,-3))
ax.set_ylim(ylim)
ax.set_xlim([n_init, steps[-1]])
ax.set_xticks([0, 50000, 150000, 250000, 350000])
plt.savefig(dcd_file_name[:-3] + 'png' ,dpi=800, bbox_inches='tight')
# plt.savefig(dcd_file_name[:-3] + 'eps' ,dpi=400, bbox_inches='tight')
# print 'View plot: \nevince %s.eps &' % dcd_file_name[:-4]
print 'View plot: \neog %s.png &' % dcd_file_name[:-4]
# plt.show()

print "mean twist angle:", mean_twist_angle.mean()
print '\m/ >.< \m/'