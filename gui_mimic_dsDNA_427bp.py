'''
Driver method to run the monte_carlo module
using DNA moves with 60 bp of dsDNA
$Id$
'''

import sys
import sassie.simulate.monte_carlo.monte_carlo as monte_carlo
# import monte_carlo
import sassie.interface.input_filter as input_filter
import multiprocessing

svariables = {}

#### user input ####
#### user input ####
#### user input ####

dcdfile='new_dsDNA_427bp.dcd'
pdbfile='new_dsDNA_427bp.pdb'
psffile='new_dsDNA_427bp.psf'

psf_flag = True
psf_flag = False # remove this line to enable minimization

max_steps = '5000'
energy_convergence = '1.0'
step_size = '0.002'

basis_string_array = []
post_basis_string_array = []
case = 1
overlap_basis = 'heavy'
if 1 == case:
    runname = 'run_5'
    number_of_flexible_regions = '1'

    basis_string_array.append('(segname DNA1 and resid > 1 and resid < 427) or '
                              '(segname DNA2 and resid > 1 and resid < 427)')
    delta_theta_array = '10.0'
    rotation_type_array = ['double_stranded_nucleic_torsion']
    rotation_direction_array = ['forward']

    post_basis_string_array.append('(segname DNA1 and resid 427) or '
                                   '(segname DNA2 and resid 1)')

temperature = '300.0'
trial_steps = '30'
goback = '50'

low_rg_cutoff = '0'
high_rg_cutoff = '800.0'

z_flag = False
z_cutoff = '0.0'

constraint_flag = False
constraint_file = 'no_such_file.txt'

directed_mc = '0'

nonbondflag = '0' # not sure what this is for
seed = '0, 123'  # set this to '1,123' if you want to set the seed or '0,123' if not

## # Specialized/Advanced Inputs
## svariables['bp_per_bead']   = ('1', 'int') # set to N > 1 to have N base-pairs coarse-grained into 1 bead
## svariables['softrotation']  = ('1', 'int') # set to N > 1 to apply rotations averaged over N coars-grained beads
## svariables['n_dcd_write']   = ('1', 'int') # set to N > 1 to only record structures after every N trials
## svariables['delta_theta_z_array'] = ('5.0,5.0','string')
## svariables['write_flex']    = ('False', 'bool') # 'True' will generate a file labeling paird DNA base pairs for all flexible beads (useful for calculating dihedral angles)
## svariables['keep_cg_files'] = ('False', 'bool') # 'True' will keep the coarse-grain DNA and protein pdb and dcd files
## svariables['keep_unique']   = ('True', 'bool') # 'False' will keep duplicate structure when move fails
## svariables['rm_pkl']        = ('False', 'bool') # 'True' will remove the coarse-grain pkl file forcing a re-coarse-graining (can be time consuming often necessary)
## svariables['openmm_min']    = ('False', 'bool') # 'True' will remove the coarse-grain pkl file forcing a re-coarse-graining (can be time consuming often necessary)

#### end user input ####
#### end user input ####
#### end user input ####


svariables['runname']                    = (runname, 'string')
svariables['dcdfile']                    = (dcdfile, 'string')
svariables['pdbfile']                    = (pdbfile, 'string')
svariables['psffile']                    = (psffile, 'string')
svariables['psf_flag']                   = (psf_flag, 'string')

svariables['max_steps']                  = (max_steps, 'int')
svariables['energy_convergence']         = (energy_convergence, 'float')
svariables['step_size']                  = (step_size, 'float')

svariables['number_of_flexible_regions'] = (number_of_flexible_regions, 'int')
svariables['basis_string_array']         = (basis_string_array, 'string')
svariables['delta_theta_array']          = (delta_theta_array, 'float_array')
svariables['rotation_type_array']        = (rotation_type_array, 'string')
svariables['rotation_direction_array']   = (rotation_direction_array, 'string')
svariables['overlap_basis']              = (overlap_basis, 'string')
svariables['post_basis_string_array']    = (post_basis_string_array, 'string')
svariables['temperature']                = (temperature, 'float')
svariables['trial_steps']                = (trial_steps, 'int')
svariables['goback']                     = (goback, 'int')
svariables['directed_mc']                = (directed_mc, 'float')

svariables['low_rg_cutoff']              = (low_rg_cutoff, 'float')
svariables['high_rg_cutoff']             = (high_rg_cutoff, 'float')

svariables['z_flag']                     = (z_flag, 'boolean')
svariables['z_cutoff']                   = (z_cutoff, 'float')

svariables['constraint_flag']            = (constraint_flag, 'boolean')
svariables['constraint_file']            = (constraint_file, 'string')

svariables['nonbondflag']                = (nonbondflag, 'int')
svariables['seed']                       = (seed,  'int_array')

error, variables = input_filter.type_check_and_convert(svariables)
if len(error) > 0:
    print 'error = ', error
    sys.exit()

txtQueue=multiprocessing.JoinableQueue()
simulation = monte_carlo.simulation()
simulation.main(variables, txtQueue)
this_text = txtQueue.get(True, timeout=0.1)

