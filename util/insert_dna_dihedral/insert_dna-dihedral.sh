#!/bin/bash
#
# Author:  Steven C. Howell
# Purpose: update the files in trunk version of SASSIE to allow for calculation dihedral angles of dsDNA
# Created: September 2014
#
# $Id: insert_dna-dihedral.sh 72 2014-10-14 20:18:58Z schowell $
#
cp mask.c /home/schowell/data/myPrograms/sassie/sassie_1.0/trunk/sassie/sasmol/extensions/mask/mask.c
cp dihedral_monte_carlo.py /home/schowell/data/myPrograms/sassie/sassie_1.0/trunk/sassie/simulate/monte_carlo/monomer/dihedral_monte_carlo.py
cp dihedral_rotate.py /home/schowell/data/myPrograms/sassie/sassie_1.0/trunk/sassie/simulate/monte_carlo/monomer/dihedral_rotate.py
