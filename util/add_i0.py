#!/usr/bin/env python
#
# Author:  Steven C. Howell
# Purpose: add I(0) into data files
# Created: 20 October 2015
#
# $Id: $
#
#0000000011111111112222222222333333333344444444445555555555666666666677777777778
#2345678901234567890123456789012345678901234567890123456789012345678901234567890

import glob
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import x_dna.util.sassie_fits as sassie_fits
import x_dna.util.gw_plot as gwp

debug = False

# get the I(0) values
df = sassie_fits.load_rg_csv()

# get the data files
data_dir = '/home/schowell/Dropbox/gw_phd/paper_tetranucleosome/1406data/iqdata/'
giq_dir = data_dir + ''
coarse_dir = data_dir + 'coarse_grid/'
coarse_ext = '*.wi0'
data_files = glob.glob(giq_dir + '*.giq')

# load each file, insert I(0), then save a new file
for data_file in data_files:
    data = np.loadtxt(data_file, skiprows=1)

    # get the coarse data file
    f_name = os.path.split(data_file)[1].split('.')[0].replace('_zeroCon','')
    coarse_file = glob.glob(coarse_dir + f_name + coarse_ext)
    assert len(coarse_file) == 1, 'ERROR: not sure which coarse file to use'
    coarse_data = np.loadtxt(coarse_file[0])

    # extract the i0 value and generate the ouput
    i0 = [0.0, df.loc[f_name]['I0 gr'], df.loc[f_name]['I0Er gr']]
    data = np.vstack((i0, data))

    # interpolate the data to the coarse grid
    new_data = sassie_fits.interp_iq(data[:,:2], coarse_data)
    new_data = np.hstack((new_data, coarse_data[:,2:]))
    new_data[0, 2] = df.loc[f_name]['I0Er gr']

    out_name = coarse_dir + f_name + '.giq0'
    np.savetxt(out_name, new_data)

    if debug:
        plt.figure()
        plt.errorbar(new_data[:,0], new_data[:,1], new_data[:,2], label='new_data')
        plt.errorbar(data[:,0], data[:,1], data[:,2], label='orig_data')
        plt.errorbar(coarse_data[:,0], coarse_data[:,1], coarse_data[:,2], label='coarse_data')
        plt.yscale('log')
        plt.legend()
        plt.show()

        plt.figure()
        gwp.guinier_plot(new_data, fmt='s', label='new data')
        gwp.guinier_plot(data, fmt='o', label='orig data')
        gwp.guinier_plot(coarse_data, fmt='>', label='coarse data')
        plt.legend()
        plt.show()

print '\m/ >.< \m/'
