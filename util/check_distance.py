import sassie.simulate.monte_carlo.ooverlap as overlap
import sassie.sasmol.sasmol as sasmol

cutoff = 0.8

pdbs = []
# pdb = '/home/schowell/myData/sassieRuns/orig/tetramer/flex20/c11.pdb'
pdbs.append(
    '/home/data/schowell_data/bigData/sassieRuns/orig/tetramer/validate_c11/run_0/energy_minimization/c11_min_run_0.dcd.pdb')
pdbs.append(
    '/home/schowell/code/pylib/x_dna/build_mol/c11_v2/c11_val/minimization/c11_min.pdb')
# pdb = '/home/schowell/code/pylib/x_dna/build_mol/trimer/psfgen_result/3x167.pdb'
pdbs.append(
    '/home/schowell/code/pylib/x_dna/build_mol/trimer/validate_3x167/run_0/energy_minimization/3x167_min_run_0.dcd.pdb')
pdbs.append(
    '/home/schowell/myData/sassieRuns/3x167_k010/run_0/energy_minimization/3x167_k010_min.dcd.pdb')
pdbs.append(
    '/home/schowell/myData/sassieRuns/3x167_k050/run_0/energy_minimization/3x167_k050_min.dcd.pdb')
pdbs.append(
    '/home/schowell/myData/sassieRuns/3x167_k100/run_0/energy_minimization/3x167_k100_min.dcd.pdb')
pdbs.append(
    '/home/schowell/myData/sassieRuns/3x167_k200/run_0/energy_minimization/3x167_k200_min.dcd.pdb')
pdbs.append(
    '/home/schowell/myData/sassieRuns/4x167_k010/run_0/energy_minimization/4x167_k010_min.dcd.pdb')
pdbs.append(
    '/home/schowell/myData/sassieRuns/4x167_k050/run_0/energy_minimization/4x167_k050_min.dcd.pdb')
pdbs.append(
    '/home/schowell/myData/sassieRuns/4x167_k100/run_0/energy_minimization/4x167_k100_min.dcd.pdb')
pdbs.append(
    '/home/schowell/myData/sassieRuns/2x167_k010/run_0/energy_minimization/2x167_k010_min.dcd.pdb')
pdbs.append(
    '/home/schowell/myData/sassieRuns/validate_2x167/run_0/energy_minimization/2x167_min_run_0.dcd.pdb')

for pdb in pdbs:
    full_molecule = sasmol.SasMol(0)
    full_molecule.read_pdb(pdb)
    overlap_basis_filter = 'not name[i] == "None" '
    # overlap_basis_filter = 'not name[i][0] == "H" '
    error, overlap_basis_mask = full_molecule.get_subset_mask(
        overlap_basis_filter)
    if error:
        print error

    error, basis_coor = full_molecule.get_coor_using_mask(
        0, overlap_basis_mask)
    if error:
        print error

    flag = overlap.ooverlap(basis_coor[0], float(cutoff))
    if flag:
        print 'FAIL: %s has atoms closer than %f' % (pdb, cutoff)
    else:
        print 'PASS: %s has no overlapping atoms' % pdb
