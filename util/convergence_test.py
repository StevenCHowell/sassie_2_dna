import numpy
import time
import sassie.sasmol.sasmol as sasmol
import matplotlib.pylab as plt
import matplotlib.gridspec as gridspec

def get_voxel_number(coor, delta):
    idx = int(coor[0]/delta)
    idy = int(coor[1]/delta)
    idz = int(coor[2]/delta)
    return (idx, idy, idz)

def get_number_new_voxels(coors, voxel_set, delta):
    number_new_voxels = 0
    for coor in coors:
        voxel_number = get_voxel_number(coor, delta)
        if voxel_number not in voxel_set:
            number_new_voxels += 1
            voxel_set.add(voxel_number)
    return number_new_voxels

def calc_convergence_all(pdb_file_name, dcd_file_names, output_prefix=None,
                         voxel_size=5.0, show=False):

    if not output_prefix:
        output_prefix = pdb_file_name[:4]

    # initialize data sets
    list_new_voxels = []
    list_occupied_voxels = []

    calculate_or_load_voxels(pdb_file_name, dcd_file_names, list_new_voxels,
                             list_occupied_voxels, voxel_size)

    n_structures = sum([len(new_voxels) for new_voxels in list_new_voxels])
    new_voxels = numpy.zeros((n_structures, 2))
    occupied_voxels = numpy.zeros((n_structures, 2))
    new_voxels[:, 0] = numpy.arange(n_structures)
    occupied_voxels[:, 0] = numpy.arange(n_structures)
    for i in xrange(len(dcd_file_names)):
        rows = list_new_voxels[i][:, 0]
        new_voxels[rows, 1] = list_new_voxels[i][:, 1]
        occupied_voxels[rows, 1] = list_occupied_voxels[i][:, 1]

    output_plot_prefix = output_prefix + '_all'
    plot_convergence(new_voxels, dcd_file_names, occupied_voxels,
                     output_plot_prefix, show)


def calc_convergence_dcd(pdb_file_name, dcd_file_names, output_prefix=None,
                         voxel_size=5.0, show=False):

    if not output_prefix:
        output_prefix = pdb_file_name[:4]

    # initialize data sets
    list_new_voxels = []
    list_occupied_voxels = []

    calculate_or_load_voxels(pdb_file_name, dcd_file_names, list_new_voxels,
                             list_occupied_voxels, voxel_size)

    n_structures = sum([len(new_voxels) for new_voxels in list_new_voxels])
    new_voxels = numpy.zeros((n_structures, len(dcd_file_names)+1))
    occupied_voxels = numpy.zeros((n_structures, len(dcd_file_names)+1))
    new_voxels[:, 0] = numpy.arange(n_structures)
    occupied_voxels[:, 0] = numpy.arange(n_structures)
    for i in xrange(len(dcd_file_names)):
        rows = list_new_voxels[i][:, 0]
        new_voxels[rows, i+1] = list_new_voxels[i][:, 1]
        occupied_voxels[rows, i+1] = list_occupied_voxels[i][:, 1]

    output_plot_prefix = output_prefix + '_dcd'
    plot_convergence(new_voxels, dcd_file_names, occupied_voxels,
                     output_plot_prefix, show)


def calculate_or_load_voxels(pdb_file_name, dcd_file_names, list_new_voxels,
                             list_occupied_voxels, voxel_size=5.0):

    # initialize molecule and mask
    mol=sasmol.SasMol(0)
    mol.read_pdb(pdb_file_name)
    n_dcds = len(dcd_file_names)
    error, mask = mol.get_subset_mask('name[i]=="CA" or name[i]=="P"')
    assert not error, error
    voxel_set = set([])
    number_occupied_voxels = 0

    tic = time.time()
    i_frame = 0
    for (i_dcd, dcd_file_name) in enumerate(dcd_file_names):
        print 'processing dcd: %s\n' % dcd_file_name
        dcd_output_prefix = '%s_%d_of_%d' % (dcd_file_name[:-4], i_dcd, n_dcds)
        output_new_voxels = dcd_output_prefix + '_new_voxels.npy'
        output_occupied_voxels = dcd_output_prefix + '_occupied_voxels.npy'

        try:
            # try loading output from previous run
            this_dcd_new_voxels = numpy.load(output_new_voxels)
            this_dcd_occupied_voxels = numpy.load(output_occupied_voxels)
            print ('Successfully loaded new voxels and occupied voxels '
                   'for %s from:\n%s \n%s' % (dcd_file_name,
                                              output_new_voxels,
                                              output_occupied_voxels))
        except:
            # calculate and create output
            print ('Calculating convergence, did not find output files from '
                   'previous calculation')
            dcd_file = mol.open_dcd_read(dcd_file_name)
            number_of_frames = dcd_file[2]
            this_dcd_new_voxels = numpy.zeros((number_of_frames, 2),
                                              dtype=int)
            this_dcd_occupied_voxels = numpy.zeros((number_of_frames, 2),
                                                   dtype=int)
            for nf in xrange(number_of_frames):
                mol.read_dcd_step(dcd_file, nf)
                error, coor = mol.get_coor_using_mask(0, mask)
                assert not error, error
                number_new_voxels = get_number_new_voxels(coor[0], voxel_set,
                                                          voxel_size)
                number_occupied_voxels += number_new_voxels

                this_dcd_new_voxels[nf, :] = [i_frame, number_new_voxels]
                this_dcd_occupied_voxels[nf, :] = [i_frame,
                                                   number_occupied_voxels]
                i_frame += 1
            print '\n'
            numpy.save(output_new_voxels, this_dcd_new_voxels)
            numpy.save(output_occupied_voxels, this_dcd_occupied_voxels)

        list_new_voxels.append(this_dcd_new_voxels)
        list_occupied_voxels.append(this_dcd_occupied_voxels)

    toc = time.time() - tic
    print "\ntime used: ", toc


def plot_convergence(new_voxels, dcd_file_names, occupied_voxels,
                     output_prefix, show=False):
    fig = plt.figure(figsize=(6, 10))
    gs = gridspec.GridSpec(2, 1, left=0.1, right=0.9, wspace=0, hspace=0)
    ax=[]
    ax.append(plt.subplot(gs[0]))
    ax.append(plt.subplot(gs[1]))
    n_plots = new_voxels.shape[1] - 1
    for i in xrange(n_plots):
        if 1 < n_plots < 100:
            label = dcd_file_names[i]
        else:
            label = ''
        if i > 0:
            rows = (new_voxels[:, i+1] > 0)
            ax[0].plot(new_voxels[rows, 0], new_voxels[rows, i+1],
                       label=label)
        else:
            rows = (new_voxels[:, i+1] > 0)[1:] # skip the initial frame
            ax[0].plot(new_voxels[rows, 0], new_voxels[rows, i+1],
                       label=label)
    ax[0].set_ylabel('Number of New Voxels')
    ax[0].xaxis.set_ticklabels([])
    if n_plots > 1 :
        lg = ax[0].legend(bbox_to_anchor=(1, 1), loc=2)
        # lg.draw_frame(False)

    for i in xrange(n_plots):
        if i > 0:
            rows = (occupied_voxels[:, i+1] > 0)
            ax[1].plot(occupied_voxels[rows, 0], occupied_voxels[rows, i+1])
        else:
            rows = (occupied_voxels[:, i+1] > 0)[1:] # skip the initial frame
            ax[1].plot(occupied_voxels[rows, 0], occupied_voxels[rows, i+1])
    ax[1].set_xlabel('Structures')
    ax[1].set_ylabel('Number of Occupied Voxels')
    ylim = ax[1].get_ylim()
    ax[1].set_ylim((ylim[0], ylim[1] * 1.1))
    plot_name = output_prefix[:-4] + '_convergence'
    plt.savefig(plot_name + '.eps', dpi=400, bbox_inches='tight')
    plt.savefig(plot_name + '.png', dpi=400, bbox_inches='tight')
    if show:
        plt.show()

if __name__=='__main__':
    import sys

    mol=sasmol.SasMol(0)
    if len(sys.argv)<3:
        mol.read_pdb('min_dsDNA60.pdb')
        # mol.read_dcd('run3_100k_ngb/monte_carlo/min_dsDNA60.dcd')
        dcd_full_name = 'run3_100k_ngb/monte_carlo/min_dsDNA60_sparse.dcd'
    else:
        mol.read_pdb(sys.argv[1])
        dcd_full_name = sys.argv[2]

    voxel_set = set([])
    delta = 5.0
    list_number_new_voxels = []
    list_number_occupied_voxels = []
    number_occupied_voxels = 0
    error, mask = mol.get_subset_mask('name[i]=="CA" or name[i]=="P"')

    dcd_file = mol.open_dcd_read(dcd_full_name)
    number_of_frames = dcd_file[2]

    tic = time.time()
    output_file = "number_of_occupied_voxels.txt"
    fout = open(output_file, 'w')
    fout.write("#frame_number, number_of_occupied_voxels\n")

    for nf in xrange(number_of_frames):
        mol.read_dcd_step(dcd_file, nf+1)
        error, coors = mol.get_coor_using_mask(0, mask)
        assert not error, error
        number_new_voxels = get_number_new_voxels(coors[0], voxel_set, delta)
        number_occupied_voxels += number_new_voxels
        list_number_new_voxels.append(number_new_voxels)
        list_number_occupied_voxels.append(number_occupied_voxels)
        fout.write("%d %d\n"%(nf, number_occupied_voxels))
    fout.close()
    toc = time.time() - tic
    print "\ntime used: ", toc

    fig = plt.figure(figsize=(6, 6))
    gs = gridspec.GridSpec(2, 1, left=0.2, right=0.95, wspace=0, hspace=0)
    ax=[]
    ax.append(plt.subplot(gs[0]))
    ax.append(plt.subplot(gs[1]))
    ax[0].plot(range(len(list_number_new_voxels)), list_number_new_voxels)
    ax[0].set_xlabel('Structure')
    ax[0].set_ylabel('number of new voxels')
    ax[0].set_yscale('log') #lim([0, max(list_number_new_voxels)*1.05])
    ax[0].xaxis.set_ticklabels([])

    ax[1].plot(range(len(list_number_occupied_voxels)), list_number_occupied_voxels)
    ax[1].set_xlabel('Structure')
    ax[1].set_ylabel('number of occupied voxels')
    ylim = ax[1].get_ylim()
    ax[1].set_ylim((ylim[0], ylim[1] * 1.1))
    plt.savefig('metric_convergence.eps', dpi=400, bbox_inches='tight')
    plt.savefig('metric_convergence.png', dpi=400, bbox_inches='tight')
    plt.show()

    print '\m/ >.< \m/'