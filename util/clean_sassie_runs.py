import os.path as op
import glob
import subprocess
from x_dna.util.sassie_fits import mkdir_p

dirs_to_move = glob.glob('/home/schowell/data/myData/sassieRuns/*/*/*/dna_mc/')
for run_dir in dirs_to_move:
    dest_dir = op.split(op.split(run_dir.replace('sassieRuns', 'bkSassieRuns')
                                 )[0])[0]
    mkdir_p(dest_dir)
    bash_cmd = 'mv %s %s' % (run_dir, dest_dir)
    print bash_cmd
    subprocess.call(bash_cmd.split())

print '\m/ >.< \m/'
